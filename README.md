# Setup Instructions

Now configured with spring boot backend please follow instructions [here](https://spring.io/guides/tutorials/spring-security-and-angular-js/) and [here](https://github.com/dsyer/spring-boot-angular)
to configure angular, spring, and install node modules.

To import into IntelliJ IDEA import project and select the pom.xml and IntelliJ will handle the rest.

After that select the run on the top right and select edit configuration, under spring boot, select RedistrictingApplication, in working directory
browse and select the base git directory. If all works out you can first test by opening a cmd prompt in the git folder and typing "mvnw spring-boot:run"
Another test is running from IntelliJ and checking localhost:8080

In both cases the console will have a password generated from spring security and will prompt for a login

Username: user

Password: text from console

After that it should show the UI mockup for redistricting.

# ProjectTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
