from matplotlib import pyplot as plt

import sys

import pandas as pd
import geopandas as gp
import re
import shapely
import json


#adds AUTOGEN_CT_DISTRICT column to table using TOWN column
from CT.ct_towns_to_districts import towns_to_districts
def ct_add_districts(df_raw):
    df_raw['AUTOGEN_CT_DISTRICT'] = df_raw.TOWN.apply(
            lambda x : towns_to_districts.get(x,None))
    return df_raw

from CO.precDestDict import getDistForPrec
def co_add_districts(df_raw):
    df_raw['AUTOGEN_CO_DISTRICT'] = df_raw['GEOID10'].apply(
        lambda x: getDistForPrec(x))
    #df_raw['AUTOGEN_CO_DISTRICT'] = getDistForPrec(df_raw['GEOID10'])
    return df_raw

def ks_add_districts(df_raw):
    df_raw['AUTOGEN_CO_DISTRICT'] = df_raw['GEOID10'].apply(
        lambda x: getDistForPrec(x))
    return df_raw

OUTFOLDER='./final_data/'
BUILD_FOLDER='./tmp/'

ALL_CONFIGS = {
"CT": {
    "state": "CT",
    "stateId": "09",
    "infile": './CT/dataverse_files/ct_final.shp',
    "outfile": './final_data/ct_out.csv',
    "out_neighbors": './final_data/ct_neighbors.csv',
    'process_raw': ct_add_districts,
    "properties_map": {
        "GEOID10": "id",

        "NAME10": "name",
        "COUNTYFP10": "county", 

        # dataset didn't actually have districts, so we add them
        # in the ct_add_districts
        "AUTOGEN_CT_DISTRICT": "district_id",

        "POP100": "population",
        "VAP": "voting_population",
	"DEMOCRAT": "democrats",
	"REPUBLICAN": "republicans",

        "geometry": "geometry",
        "stateId": "state_id",
        },
    },
"CO": {
    "state": "CO",
    "stateId": "08",
    "infile": './CO/dataverse_files/CO_final.shp',
    "outfile": './final_data/co_out.csv',
    "out_neighbors": './final_data/co_neighbors.csv',
    'process_raw': co_add_districts,
    "properties_map": {
        "GEOID10": "id",
	"COUNTYFP10": "county",

	"AUTOGEN_CO_DISTRICT": "district_id",

	"POP100": "population",
        "VAP": "voting_population",
        "NDV": "democrats",
	"NRV": "republicans"
        },
    },
"KS": {
    "state": "KS",
    "stateId": "20",
    "infile": './KS/dataverse_files/KS_final.shp',
    "outfile": './final_data/ks_out.csv',
    "out_neighbors": './final_data/ks_neighbors.csv',
    'process_raw': co_add_districts,
    "properties_map": {
        "GEOID10": "id",

        "NAME10": "name",
        "COUNTYFP10": "county",

        "AUTOGEN_CO_DISTRICT": "district_id",

        "POP100": "population",
        "VAP": "voting_population",
        "NDV": "democrats",
	"NRV": "republicans",

	"INTPTLAT10": "lat",
	"INTPTLON10": "lon",

        "geometry": "geometry",
        "stateId": "state_id",
        },
    }
}



# ================== CHANGE THIS TO CHANGE WHICH IT GENERATES
# TODO, maybe just loop through all of em
args = sys.argv[1:]
if(len(args) == 0):
    print("Please enter a state: " + str(list(ALL_CONFIGS.keys())))
    exit(-1)
elif(len(args) > 1):
    print("Please enter only 1 state: " + str(list(ALL_CONFIGS.keys())))
    exit(-1)
else:
    statecode = args[0].upper()
    if(statecode not in ALL_CONFIGS):
        print("Unrecognized statecode '{}'".format(statecode))
        print("Please enter a state: " + str(list(ALL_CONFIGS.keys())))
        exit(-1)

CURR_CONFIG = ALL_CONFIGS[statecode]




# CODE IS HAPPENING BELOW HERE, LEAVE CONFIG STUFF ABOVE HERE
# ====================================================================================
# ====================================================================================
# ====================================================================================
# ====================================================================================

print("Loading from {}".format(CURR_CONFIG['infile']), flush=True)
df_raw = gp.read_file(CURR_CONFIG['infile'])

df_raw['state'] = CURR_CONFIG['state']

# =============== get rid of weird empty districts =============
# There's a handful of districts with no population, no voting district id
# and theyre big and empty and over water
#undef_dists = df_raw['NAME10'].str.contains(
#                'voting districts not defined', 
#                flags=re.I)
#df_raw = df_raw[~ undef_dists]



# ============= discard extra columns, rename  =================
print("Selecting/renaming columns", flush=True)

def noop(x): return x
preproc_func = CURR_CONFIG.get("process_raw", noop)
df_raw = preproc_func(df_raw)

prop_map = CURR_CONFIG["properties_map"]
df = df_raw.reindex(columns=prop_map.keys())
df = df.rename(columns=prop_map)
df = df.set_index('id')

# ================ assign districts = None to null district for state ===
#df.loc[df.district_id.isnull(), 'district_id'] = CURR_CONFIG["state"] + "_NULL"


# ================= Add neighbors column ==========================
#print("Calculating neighbors", flush=True)

#geoms = df[['geometry']]

#join geoms to the geoms they're intersecting
#index will contain duplicate entries
#columns will be: geometry, index_right
#t_joined = gp.sjoin(geoms, geoms, how='left')

# take just index column, then filter out self neightbors
#t_indices = t_joined["index_right"]
#t_precinct_to_precinct = t_indices[t_indices != t_indices.index]

#print("Got index")
#print(t_precinct_to_precinct)


# ============ Put neighbors into a comma-delimited neighbors column

#group duplicate entries, 
#t_groups = t_precinct_to_precinct.groupby(t_precinct_to_precinct.index)

#result is series of index: 'neighb,neighb,...'
#filter out (entries == self)
#neighbors = t_groups.apply(lambda subf: ','.join(subf))

#Got the neighbors
#df['neighbors'] = neighbors


# ============== output neighbors in separate neighbors table (precinct_to_precinct)

#t_joined
#neighbors_outfile = CURR_CONFIG["out_neighbors"]
#print("Outputting neighbor data to {}".format(neighbors_outfile))
#t_precinct_to_precinct.to_csv(neighbors_outfile);


# ======================== PRETTY MUCH DONE
#NOW:
#do topological simplification with topojson scripts (shell commands)
#1: store a copy of the topojson in the out_dir
#2: store a temp copy of the simplified geojson
#3: read that in, and output as geopandas csv

from subprocess import call
from shutil import copyfile
import os
os.makedirs(BUILD_FOLDER, exist_ok=True) #make sure tmp exists

TEMP_CLEAN_GEOJSON_FILE = f'{BUILD_FOLDER}/{CURR_CONFIG["state"]}_clean_geojson.json'
TEMP_CLEAN_TOPO_FILE = f'{BUILD_FOLDER}/{CURR_CONFIG["state"]}_clean_topo.json'
TEMP_SIMPLIFIED_TOPO_FILE = f'{BUILD_FOLDER}/{CURR_CONFIG["state"]}_simplified_topo.json'
TEMP_SIMPLIFIED_GEOJSON_FILE = f'{BUILD_FOLDER}/{CURR_CONFIG["state"]}_simplified_geojson.json'

OUT_GEO_FILE= f'{OUTFOLDER}/{CURR_CONFIG["state"]}_geojson.json'

#Output df as geojson to tmp
#with open(TEMP_CLEAN_GEOJSON_FILE, 'w') as temp_outf:
#    temp_outf.write(df.to_json())


#convert geojson to topojson
print("Converting to topology")
#call(['npx','geo2topo', '-q', '1e8', 
#    f'precincts={TEMP_CLEAN_GEOJSON_FILE}',
#    '-o', TEMP_CLEAN_TOPO_FILE])
#npx geo2topo -q 1e8 nj=NJ_geo.json -o NJ_topo.json


#Simplify topojson
print("Simplifying topology")
#planar-quantile decides what fraction to keep
#i.e. 0.1 means keep 10% of the points
#it will always remove the easiest ones first
#0.5 looks flawless unless you zoom in reaaaal hard
#0.35 is fine
#0.2 is getting chunky in places but still looks acceptable
#call(['npx', 'toposimplify', '--planar-quantile', '0.2',  
#    TEMP_CLEAN_TOPO_FILE, 
#    '-o', TEMP_SIMPLIFIED_TOPO_FILE])


#print("Converting back to geojson")
#npx topo2geo nj=NJ_geo2.json < NJ_topo2.json
#call(['npx', 'topo2geo', 
#    '--in', TEMP_SIMPLIFIED_TOPO_FILE,
#    f'precincts={TEMP_SIMPLIFIED_GEOJSON_FILE}'])


#### We've created our files now just copy the topo file to outdir
#print("Copying topo file to output")
#copyfile(TEMP_SIMPLIFIED_GEOJSON_FILE, OUT_GEO_FILE)


# ================= FINAL STAGE, OUTPUT GEOJSON AS CSV FOR DB
# load TEMP_SIMPLIFIED_GEOJSON_FILE into geopandas, output as csv

print("Loading simplified json {}".format(TEMP_CLEAN_GEOJSON_FILE), flush=True)
#df_simple = gp.read_file(TEMP_CLEAN_GEOJSON_FILE)
#df_simple = df_simple.set_index('id')


# ============== OUTPUT ===============

# convert all geometries to json
def geom_to_json(geom):
    return json.dumps(shapely.geometry.mapping(geom))

#df_simple['geometry'] = df_simple['geometry'].apply(geom_to_json)

#df is still a geodatafram, but there's nothing geographic about it
#out_df = pd.DataFrame(df_simple)

outfile = CURR_CONFIG["outfile"]
print("Outputting |-delimited csv to {}".format(outfile))
#out_df.to_csv(outfile, sep='|')
df.to_csv(outfile)

