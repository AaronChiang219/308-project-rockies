use jaskisingh;

SET SQL_SAFE_UPDATES=0;

delete from precinct;
delete from precinct_neighbors;
delete from state_precincts;

delete from cluster;
delete from cluster_precincts;
delete from cluster_neighbors;
delete from cluster_edge;
delete from cluster_edges;
delete from state_clusters;

delete from voter_info;

SET SQL_SAFE_UPDATES=1;