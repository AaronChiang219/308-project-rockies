FileReader prec = null;
    try {
      prec = new FileReader("coprecincts.csv");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    GeometryFactory geometryFactory = new GeometryFactory();

    HashMap<String, Geometry> precincts = new HashMap<>();

    try {
      String line;
      BufferedReader br = new BufferedReader(prec);
      line = br.readLine();
      while ((line = br.readLine()) != null) {
        String[] values = line.split(",");
        Geometry g = geometryFactory.createPoint(new Coordinate(Double.parseDouble(values[2]), Double.parseDouble(values[1])));
        precincts.put(values[0], g);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    List<Geometry> geos = new ArrayList<>();

    FileReader r = null;
    try {
      r = new FileReader("codistricts.json");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    BufferedReader reader = new BufferedReader(r);

    String currentLine = null;
    try {
      currentLine = reader.readLine();

      while (currentLine != null) {
//        currentLine = currentLine.substring(0, currentLine.length()-1);
        GeoJSONReader geoReader = new GeoJSONReader();
        Geometry geo = geoReader.read(currentLine);
        geos.add(geo);
        currentLine = reader.readLine();
      }
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    HashMap<String, String> dis = new HashMap<>();

    for (String p: precincts.keySet()
         ) {
      int i = 1;
      Geometry point = precincts.get(p);
      for (Geometry g: geos
           ) {
        if(point.within(g)){
          dis.put(p, "CO0" + i);
          break;
        }
        i++;
      }
    }

    try (PrintWriter writer = new PrintWriter(new File("precDistMapping.csv"))) {

      StringBuilder sb = new StringBuilder();
      sb.append("id");
      sb.append(',');
      sb.append("did");
      sb.append('\n');

      for (String s: dis.keySet()
           ) {
        sb.append(s);
        sb.append(',');
        sb.append(dis.get(s));
        sb.append('\n');
      }

      writer.write(sb.toString());

      System.out.println("done!");

    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }


    System.out.println(geos);

