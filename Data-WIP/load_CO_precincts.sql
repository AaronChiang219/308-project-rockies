use jaskisingh;

insert into precinct(id, county, population, voting_population, democrats, republicans, shape)
select c.id, c.county, c.population, c.voting_population, c.democrats, c.republicans, cg.geometry
from co_out5 c
left join co_out22 cg
on c.id = cg.id;

insert into state_precincts
select '8', id
from co_out5;

insert into precinct_neighbors
select id, nid
from co_neighbors;