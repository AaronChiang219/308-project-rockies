use jaskisingh;

insert into precinct(id, county, population, voting_population, democrats, republicans, shape)
select id, county, population, voting_population, democrats, republicans, geometry
from ct_final;

insert into state_precincts
select '9', id
from ct_final;

insert into precinct_neighbors
select id, neighbor
from ct_neighbors;