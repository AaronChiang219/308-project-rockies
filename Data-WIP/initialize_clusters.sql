use jaskisingh;

insert into cluster
select id, population
from precinct;

insert into cluster_precincts
select c.id, p.id
from cluster c
left join precinct p
on c.id=p.id;

insert into cluster_neighbors
select precinct_id, neighbors_id
from precinct_neighbors;

insert into cluster_edge(a_id, b_id, county)
select precinct_id, neighbors_id,
case
when p.county=pn.county then 0.9
else 0.1
end
from precinct_neighbors n
left join precinct p on p.id = n.precinct_id
left join precinct pn on pn.id = n.neighbors_id;

insert into cluster_edges(cluster_id, edges_id)
select a_id, id
from cluster_edge;

# associate clusters with state
# will be updated once precincts have states column

insert into state_clusters
select 9, id
from precinct;
