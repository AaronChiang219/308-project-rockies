import {MatTable} from '@angular/material';
import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AppService, Status} from '../app.service';
import {HttpClient} from '@angular/common/http';
import {Control, DomUtil, geoJSON, latLng, Layer, Map as leafMap, tileLayer} from 'leaflet';
import {Feature, GeoJsonProperties, GeometryObject} from "geojson";
import * as style from "./leaflet.component.theme";
import {timer} from "rxjs";
import {BatchSummary} from "./batchsummary";

const CT = '09';
const CO = '08';
const KS = '20';

const initialView = latLng(37.09, -95.71);
const initialZoom = 4;

const polling$ = timer(0, 150);
const initialDistrictData = {
  population: 0,
  democrats: 0,
  republicans: 0
};

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-leaflet',
  templateUrl: './leaflet.component.html',
  styleUrls: ['./leaflet.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class LeafletComponent implements OnInit {

  leafletMap: leafMap;
  mapList = [];
  state: number;

  @ViewChild(MatTable) table: MatTable<BatchSummary>;
  displayedColumns: string[] = ['run', 'democratic', 'republican', 'objectiveFunctionValues', 'PolsbyPopper', 'Schwartzberg', 'equalPopulation', 'efficiencyGap', 'meanMedianDifference1', 'meanMedianDifference2'];
  dataSource: BatchSummary[] = [];

  precinctConfig = {
    style: this.precinctColors,
    onEachFeature: (feature: Feature<GeometryObject, GeoJsonProperties>, layer: Layer) => {
      layer.on(
        {
          mouseover: event1 => {event1.target.setStyle(style.mouseover);
            this.showPrecinctData(event1.target);
            this.showDistrictData(event1.target);},
          mouseout: event1 => {event1.target.setStyle(this.precinctColors(event1.target.feature));},
          click: event1 => {},
        });
    }
  };

  districtConfig = {
    style: this.districtColors,
    onEachFeature: (feature: Feature<GeometryObject, GeoJsonProperties>, layer: Layer) => {
      layer.on(
        {
          mouseover: event1 => {event1.target.setStyle(style.mouseover);},
          mouseout: event1 => {event1.target.setStyle(this.districtColors(event1.target.feature));},
          click: event1 => {this.leafletMap.fitBounds(event1.target.getBounds());
                            this.loadDetailedData("Precinct");}
        });
    }
  };

  stateConfig = {
    style: style.mouseout,
    onEachFeature: (feature: Feature<GeometryObject, GeoJsonProperties>, layer: Layer) => {
      layer.on(
        {
          mouseover: event1 => {event1.target.setStyle(style.mouseover);},
          mouseout: event1 => {event1.target.setStyle(style.mouseout);},
          click: event1 => {this.state = event1.target.feature.id;
                            this.loadDetailedData("District");}
        });
    }
  };

  customControl = class displayControl extends Control{
    div: HTMLElement;
    onAdd(map: leafMap){
      this.div = DomUtil.create("div", 'info');
      this.div.innerHTML = "<p> District Information: Please Zoom To Load...</p>";
      return this.div;
    }
    onRemove(map: leafMap){
      DomUtil.remove(this.div);
    }
  };

  districtPopup = new this.customControl({
    position: 'topright',
  });

  statePopup = new this.customControl({
    position: 'bottomright',
  });

  ctPrecincts = geoJSON(require("../../assets/CT_geojson.json"), this.precinctConfig);
  coPrecincts = geoJSON(require("../../assets/CO_geojson.json"), this.precinctConfig);
  ksPrecincts = geoJSON(require("../../assets/KS_geojson.json"), this.precinctConfig);
  ctDistricts = geoJSON(require("../../assets/ctdistricts.json"), this.districtConfig);
  coDistricts = geoJSON(require("../../assets/codistricts.json"), this.districtConfig);
  ksDistricts = geoJSON(require("../../assets/ksdistricts.json"), this.districtConfig);
  statesData = geoJSON(require("../../assets/states.json"), this.stateConfig);

  precinctMap = new Map();
  districtMap = new Map();
  districtInfoMap = new Map();
  colorMap = new Map();

  layerMap = new Map();
  algorithmMap = new Map();
  stateMap = new Map();
  graphUpdates = [];
  simUpdates = [];
  currentPrecincts = null;
  paused: boolean = false;

   options = {
    layers: [
      tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> ' +
                     'contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                     'Imagery © <a href="http://mapbox.com">Mapbox</a>',
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoibWF0aWphbWF0aWphIiwiYSI6ImNqOW9iMXVvNDFjZWkyd250YWt5N293NDgifQ.17Rp_cKDonZArEnkqvDGXg'
      } as any),
    ],
    zoom: initialZoom,
    center: initialView
  };

  constructor(private app: AppService, private http: HttpClient) {
    this.stateMap.set(CT, "CT");
    this.stateMap.set(CO, "CO");
    this.stateMap.set(KS, "KS");
    this.precinctMap.set(CT, this.ctPrecincts);
    this.precinctMap.set(CO, this.coPrecincts);
    this.precinctMap.set(KS, this.ksPrecincts);
    this.districtMap.set(CT, this.ctDistricts);
    this.districtMap.set(CO, this.coDistricts);
    this.districtMap.set(KS, this.ksDistricts);
  }

  hashCode(str) {
    return str.split('').reduce((prevHash, currVal) =>
      (((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
  }

  ngOnInit(){
    polling$
      .subscribe(() => this.checkForUpdates());
  }

  checkForUpdates(){
    switch(this.app.status){
      case Status.SETUP:{
        this.clearDistrictData();
        this.app.status = Status.ALGORITHM;
        break;
      }
      case Status.ALGORITHM:{
        this.http.get("api/algorithm/update/graph")
          .subscribe((data) => {
              if (data!=null){
                this.graphUpdates.push(data);
              }
              if (!this.paused){
                this.updateGraphPartition();
              }
            },
            error => {
              console.log(error);
            });
        break;
      }
      case Status.SIMULATED:{
        this.http.get("api/algorithm/update/sim")
          .subscribe((data) => {
              if (data!=null){
                this.simUpdates.push(data);
              }
              if (!this.paused){
                this.updateSimulatedAnnealing();
              }
            },
            error => {
              console.log(error);
            });
        break;
      }
      case Status.BATCH:{
        this.http.get<Array<BatchSummary>>("api/batch/update")
          .subscribe((data: Array<BatchSummary>) => {
              data.forEach((batch: BatchSummary) =>{
                console.log(batch);
                this.dataSource.push(batch);
                this.table.renderRows();
                if (this.dataSource.length == this.app.batchRuns){
                  this.app.status = null;
                }
              })
            },
            error => {
              console.log(error);
            });
        break;
      }
    }
  }

  doUpdate(){
    if (this.app.status == Status.ALGORITHM){
      this.updateGraphPartition();
    } else if (this.app.status == Status.SIMULATED){
      this.updateSimulatedAnnealing();
    }
  }

  updateGraphPartition(){
    if (this.graphUpdates.length != 0){
      if (this.app.iteration){
        this.doGraphUpdate();
      } else {
        let data = this.graphUpdates.shift();
        if (data["status"] == 1){
          this.finishGraphPartition();
        }
      }
    }
  }

  doGraphUpdate(){
    let data = this.graphUpdates.shift();
    this.showStateData(data);
    let updates = data["updates"];
    this.pushGraphUpdates(updates);
    if (data["status"] == 1){
      this.finishGraphPartition();
    }
  }

  finishGraphPartition(){
    this.app.status = null;
    this.graphUpdates.forEach((list) =>{
      this.showStateData(list);
      let updates = list["updates"]
      this.pushGraphUpdates(updates);
    });
    this.clearDistrictData();
    this.updatePartOneDistricts();
    this.http.get("api/algorithm/start/sim").subscribe();
    this.app.status = Status.SIMULATED;
  }

  pushGraphUpdates(updates){
    updates.forEach((update) => {
      let hash = (this.hashCode(update["id"])>>>0)%20;
      this.setPrecinctColor(update["pid"], hash, update["id"]);
      this.updateDistrictInfo(update["pid"], hash);
    });
  }

  updateDistrictInfo(precinct_id, district) {
    let layer = this.layerMap.get(precinct_id);
    let info = this.districtInfoMap.get(district);
    this.addDistrictInfo(info, layer);
  }

  clearDistrictData(){
    for(let i = 0; i < 20; i++){
      this.districtInfoMap.set(i, {...initialDistrictData});
    }
  }

  updatePartOneDistricts(){
    let index = 0;
    this.currentPrecincts.eachLayer(layer => {
      let cluster_id = layer.feature.properties.cluster_id;
      if (!this.algorithmMap.get(cluster_id)){
        index++;
        this.algorithmMap.set(cluster_id, index);
        this.setPrecinctColor(layer.feature.id, index, cluster_id);
      } else {
        this.setPrecinctColor(layer.feature.id, this.algorithmMap.get(cluster_id), cluster_id);
      }
    });
    this.currentPrecincts.eachLayer(layer => {
      this.updateDistrictData(layer);
    });
  }

  updateDistrictData(layer){
    let district = this.districtInfoMap.get(parseInt((layer as any).feature.properties.district_id.slice(2,4)));
    if (district){
      this.addDistrictInfo(district, layer)
    }
  }

  updateSimulatedAnnealing(){
    if (this.simUpdates.length != 0){
      let data = this.simUpdates.shift();
      console.log(data["id"]);
      console.log(data["status"]);
      this.showStateData(data);
      let updates = data["updates"];
      this.pushSimUpdates(updates);
      if (data["status"]==1){
        this.simUpdates.forEach((list) =>{
          this.showStateData(list);
          let updates = list["updates"]
          this.pushSimUpdates(updates);
        });
        this.app.status = null;
      }
    }
  }

  pushSimUpdates(updates){
    updates.forEach((update) => {
      let district = this.algorithmMap.get(update["id"]);
      this.swapDistrictInfo(update["pid"], district);
      this.setPrecinctColor(update["pid"], district, update["id"]);
    });
  }

  swapDistrictInfo(precinct_id, new_district) {
    let layer = this.layerMap.get(precinct_id);
    let old_district = layer.feature.properties.district_id;
    let old_info = this.districtInfoMap.get(parseInt(old_district.slice(2,4)));
    let info = this.districtInfoMap.get(new_district);
    this.addDistrictInfo(info, layer);
    this.subtractDistrictInfo(old_info, layer);
  }


  addDistrictInfo(info, layer){
    info.population += layer.feature.properties.population;
    info.republicans += layer.feature.properties.republicans;
    info.democrats += layer.feature.properties.democrats;
  }

  subtractDistrictInfo(info, layer){
    info.population -= layer.feature.properties.population;
    info.republicans -= layer.feature.properties.republicans;
    info.democrats -= layer.feature.properties.democrats;
  }


  districtColors(district){
    console.log(district.properties.DISTRICT);
    let color = style.districtColors[parseInt(district.properties.DISTRICT, 10)];
    console.log(color);
    let dynamic = {...style.none, fillColor: color};
    return dynamic;
  }

  precinctColors(precinct) {
    if (precinct.properties.district_id){
      console.log(precinct.properties.district_id.slice(2,4));
      let color = style.districtColors[parseInt(precinct.properties.district_id.slice(2, 4), 10)];
      let dynamic = {...style.none, fillColor: color};
      return dynamic;
    }
    return style.none;
  }


  setPrecinctColor(precinct_id, district, cluster_id) {
    let layer = this.layerMap.get(precinct_id);
    let state = this.stateMap.get(this.state);
    layer.feature.properties.district_id = state + district;
    layer.feature.properties.cluster_id = cluster_id;
    let color = style.districtColors[district];
    let dynamic = {...style.none, fillColor: color};
    layer.setStyle(dynamic);
  }

  loadDetailedData(lod){
    if (lod=="District"){
      this.loadDistrictData();
    } else if (lod=="Precinct"){
      this.loadPrecinctData();
    }
  }

  loadDetailedDataDropdown(state){
    this.state = state.value;
    this.loadDistrictData();
  }

  loadDetailedDataZoom(event){
    console.log(this.leafletMap.getZoom());
    if (this.state && this.leafletMap.getZoom()>9){
      this.loadPrecinctData();
    }
  }

  loadDistrictData(){
    this.districtPopup.addTo(this.leafletMap);
    this.statePopup.addTo(this.leafletMap);
    this.showInitialStateData(this.state);
    this.app.stateId = this.state;
    this.leafletMap.removeLayer(this.statesData);
    let districts = this.districtMap.get(this.state);
    this.leafletMap.fitBounds(districts.getBounds());
    districts.addTo(this.leafletMap);
    districts.eachLayer(layer =>{
      this.districtInfoMap.set(parseInt((layer as any).feature.properties.DISTRICT), {...initialDistrictData});
    });
  }

  loadPrecinctData(){
    this.leafletMap.removeLayer(this.districtMap.get(this.state));
    this.currentPrecincts = this.precinctMap.get(this.state);
    this.currentPrecincts.addTo(this.leafletMap);
    this.currentPrecincts.eachLayer(layer => {
      this.layerMap.set((layer as any).feature.id, layer);
      if (!this.colorMap.get((layer as any).feature.id)){
        this.colorMap.set((layer as any).feature.id, parseInt((layer as any).feature.properties.district_id.slice(2,4)));
      }
      this.updateDistrictData(layer);
    });
  }

  showPrecinctData(precinct){
    let features = precinct.feature;
    let popupText =
      `<table>` +
      `<tr><th>ID:</th> <td>${features.id}</td> </tr> ` +
      `<tr><th>Name:</th> <td>${features.properties.name}</td> </tr>` +
      `<tr><th>District:</th> <td>${features.properties.district_id}</td> </tr>` +
      `<tr><th>Total Population:</th> <td>${features.properties.population.toLocaleString()}</td> </tr>` +
      `<tr><th>Democrat:</th> <td>${features.properties.republicans.toLocaleString()}</td> </tr>` +
      `<tr><th>Republican:</th> <td>${features.properties.democrats.toLocaleString()}</td> </tr>` +
      `</table>`;
    let popupOptions = {autoPan: false};
    precinct.bindPopup(popupText, popupOptions).openPopup();
  }

  showDistrictData(precinct){
    let index = parseInt(precinct.feature.properties.district_id.slice(2,4));
    let district = this.districtInfoMap.get(index);
    let districtText =
      `<table>` +
      `<tr><th>District Information:</th> <td>${index}</td></tr> ` +
      `<tr><th>Population:</th> <td>${Math.round(district.population).toLocaleString()}</td> </tr>` +
      `<tr><th>Democrat:</th> <td>${Math.round(district.democrats).toLocaleString()}</td> </tr>` +
      `<tr><th>Republican:</th> <td>${Math.round(district.republicans).toLocaleString()}</td> </tr>` +
      `</table>`;
    this.districtPopup.div.innerHTML = districtText;
  }

  showStateData(data){
    let stateText =
      `<table>` +
      `<tr><th>State Information:</th></tr> ` +
      `<tr><th>PolsbyPopper:</th> <td>${data["compactnessPolsbyPopper"].toPrecision(4)}</td> </tr>` +
      `<tr><th>Schwartzberg:</th> <td>${data["compactnessSchwartzberg"].toPrecision(4)}</td> </tr>` +
      `<tr><th>Population Variance:</th> <td>${data["equalPopulation"].toPrecision(4)}</td> </tr>` +
      `<tr><th>Efficiency Gap:</th> <td>${data["efficiencyGap"].toPrecision(4)}</td> </tr>` +
      `<tr><th>Objective Function:</th> <td>${data["objectiveFunction"].toPrecision(4)}</td> </tr>` +
      `</table>`;
    this.statePopup.div.innerHTML = stateText;
  }

  showInitialStateData(state){
    let info = null;
    this.statesData.eachLayer(layer =>{
      if ((layer as any).feature.id === state){
        info = (layer as any).feature.properties;
      }
    });
    let stateText =
      `<table>` +
      `<tr><th>State Information:</th></tr> ` +
      `<tr><th>PolsbyPopper:</th> <td>${info["Polsby"].toPrecision(4)}</td> </tr>` +
      `<tr><th>Schwartzberg:</th> <td>${info["Schwartzberg"].toPrecision(4)}</td> </tr>` +
      `<tr><th>Population Variance:</th> <td>${info["EqualPopulation"].toPrecision(4)}</td> </tr>` +
      `<tr><th>Objective Function:</th> <td>${info["ObjFunction"].toPrecision(4)}</td> </tr>` +
      `</table>`;
    this.statePopup.div.innerHTML = stateText;
  }


  resetView(stateSelect){
    stateSelect.value = "";
    this.leafletMap.eachLayer(layer => {
      if (!layer.getAttribution()) {
        this.leafletMap.removeLayer(layer);
      }
    });
    this.dataSource = [];
    this.newMap();
    this.leafletMap.removeControl(this.districtPopup);
    this.leafletMap.removeControl(this.statePopup);
    this.statesData.addTo(this.leafletMap);
    this.leafletMap.setView(initialView, initialZoom);
  }

  getUserMaps(){
    if (this.app.user!=null){
      this.mapList = [];
      this.http.post("api/state/maps", this.app.user)
        .subscribe((data: Array<string>) => {
          data.forEach((list) =>{
            this.mapList.push({name: parseInt(list)});
          })
      })
    }
  }

  newMap(){
    if (this.currentPrecincts){
      this.clearDistrictData();
      this.currentPrecincts.eachLayer(layer =>{
        let district = this.colorMap.get(layer.feature.id);
        this.setPrecinctColor(layer.feature.id, district, null);
        this.updateDistrictData(layer);
      });
    }
    this.showInitialStateData(this.state);
  }

  saveMap(){
    this.http.get("api/algorithm/save").subscribe();
  }

  loadMap(mapid){
    this.http.post("api/state/load", mapid)
      .subscribe((data) =>{
        let index = 0;
        let updates = data["updates"];
        this.showStateData(data);
        this.algorithmMap = new Map();
        updates.forEach((update) => {
          if (!this.algorithmMap.get(update["id"])){
            index++;
            this.algorithmMap.set(update["id"], index);
            this.setPrecinctColor(update["pid"], index, null);
          } else {
            this.setPrecinctColor(update["pid"], this.algorithmMap.get(update["id"]), null);
          }
        });
        this.clearDistrictData();
        this.currentPrecincts.eachLayer(layer => {
          this.updateDistrictData(layer);
        });
      });
  }

  deleteMap(mapid){
    this.http.post("api/state/remove", mapid)
      .subscribe();
    this.mapList = this.mapList.filter(item => item.name!==mapid);
  }

  toggleOverlay(){
    let districts = this.districtMap.get(this.state);
    if (this.leafletMap.hasLayer(districts)){
      this.leafletMap.removeLayer(districts)
    } else {
      districts.addTo(this.leafletMap);
    }
  }

  onMapReady(map: leafMap) {
    this.leafletMap = map;
  }

}
