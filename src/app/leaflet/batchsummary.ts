export class BatchSummary {

  constructor(
    public run: number,
    public mapId: number,
    public democratic: number,
    public republican: number,
    public objectiveFunctionValues: number,
    public compactnessPolsbyPoppper: number,
    public compactnessSchwartzberg: number,
    public equalPopulation: number,
    public efficiencyGap: number,
    public meanMedianDifference: Array<number>
  ) {}
}
