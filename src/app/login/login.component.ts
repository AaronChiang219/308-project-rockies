import { Component } from '@angular/core';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {finalize} from "rxjs/operators";
import {RegisterDialogComponent} from "../register-dialog/register-dialog.component";
import {MatDialog} from "@angular/material";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  username = new FormControl("", [Validators.required])
  password = new FormControl("", [Validators.required])
  credentials = {username: '', password: ''};

  constructor(private app: AppService, private http: HttpClient, private router: Router, public dialog: MatDialog) {
  }

  login() {
    this.credentials.username = this.username.value;
    this.credentials.password = this.password.value;
    this.app.authenticate(this.credentials, () => {
      this.credentials.username = '';
      this.credentials.password = '';
      this.username.reset();
      this.password.reset();
    });
    this.username.reset('');
    this.password.reset('');
    return false;
  }

  logout() {
    this.http.post('logout', {}).pipe(finalize(() => {
      this.app.authenticated = false;
    })).subscribe();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      width: '250px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getUserError(){
    return this.username.hasError('required') ? "Username Required" : "Invalid Username";
  }

  getPasswordError(){
    return this.password.hasError('required') ? "Password Required" : "Invalid Password";
  }

  authenticated() { return this.app.authenticated; }

}
