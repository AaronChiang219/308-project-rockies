import { Component, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {HttpClient} from "@angular/common/http";
export interface DialogData {
  username: string;
  password: string;
  email:string;
}
@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.css']
})
export class RegisterDialogComponent{

  user = {username: '', password: ''};

  constructor(public dialogRef: MatDialogRef<RegisterDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData, private http: HttpClient) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  registerUser(){
    this.http.post("user/register", this.user)
      .subscribe(data => {
          console.log(data['username']);
          console.log(data['password']);
          this.user.username = '';
          this.user.password = '';
          this.dialogRef.close();
        },
        error => {
          console.log("This is my error message", error);
        });
  }
}
