import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export enum Status {
  SETUP,
  ALGORITHM,
  BATCH,
  SIMULATED
}

@Injectable()
export class AppService {

  authenticated: boolean = false;
  stateId: number;
  status: Status;
  iteration: boolean = true;
  batchRuns: number;
  user: string;

  constructor(private http: HttpClient) {
  }

  /*
   * Method will create an http basic Authentication header if credentials are
   * provided, get our user if credentials are correct, and execute given callback function
   */
  authenticate(credentials, callback) {
        const headers = new HttpHeaders(credentials ? {
            authorization : 'Basic ' + btoa(credentials.username + ':' + credentials.password,),
        } : {});
        this.http.get('user', {headers: headers}).subscribe(response => {
            if (response['name']) {
              this.user = response['name'];
              console.log(this.user);
              this.authenticated = true;
            } else {
              this.user = null;
              this.authenticated = false;
            }
          return callback && callback();
        });
  }
}
