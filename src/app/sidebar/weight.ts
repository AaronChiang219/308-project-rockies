export class Weight {

  constructor(
    public stateId: number,
    public desiredDistricts: number,
    public equalPopulation: number,
    public efficiencyGap: number,
    public compactness: number,
    public schwartzberg: number,
    public polbsyPopper: number,
    public seatsToVotes: number,
    public meanMedianDifference: number,
    public iterations: number
  ) {}

  normalize(){
    let total = this.equalPopulation + this.compactness + this.meanMedianDifference + this.efficiencyGap + this.schwartzberg;
    this.equalPopulation /= total;
    this.polbsyPopper = this.compactness;
    this.polbsyPopper /= total;
    this.meanMedianDifference /= total;
    this.efficiencyGap /= total;
    this.compactness = 0;
    this.seatsToVotes = 0;
  }

  normalizeMin(max: Weight){
    let total = this.equalPopulation + max.equalPopulation + this.compactness + max.compactness + this.meanMedianDifference + max.meanMedianDifference + this.efficiencyGap + max.efficiencyGap + this.schwartzberg + max.schwartzberg;

    this.equalPopulation /= total;
    this.polbsyPopper = this.compactness;
    this.polbsyPopper /= total;
    this.compactness /= total;
    this.meanMedianDifference /= total;
    this.efficiencyGap /= total;
    this.compactness = 0;
    this.seatsToVotes = 0;
  }

  normalizeMax(min: Weight){
    let total = this.equalPopulation + min.equalPopulation + this.compactness + min.compactness + this.meanMedianDifference + min.meanMedianDifference + this.efficiencyGap + min.efficiencyGap + this.schwartzberg + min.schwartzberg;

    this.equalPopulation += min.equalPopulation;
    this.compactness += min.compactness;
    this.meanMedianDifference += min.meanMedianDifference;
    this.efficiencyGap += min.efficiencyGap;

    this.equalPopulation /= total;
    this.polbsyPopper = this.compactness;
    this.polbsyPopper /= total;
    this.compactness /= total;
    this.meanMedianDifference /= total;
    this.efficiencyGap /= total;
    this.compactness = 0;
    this.seatsToVotes = 0;
  }
}
