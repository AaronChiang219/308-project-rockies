import {ChangeDetectorRef, Component} from '@angular/core';
import {AppService, Status} from '../app.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Weight} from "./weight";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  algorithmWeight = new Weight(0, 2, 0, 0, 0, 0, 0, 0, 0, 1);
  batchMinWeight = new Weight(0, 2, 0, 0, 0, 0, 0, 0, 0, 2);
  batchMaxWeight = new Weight(0, 2, 0, 0, 0, 0, 0, 0, 0, 2);

  constructor(private app: AppService, private http: HttpClient, private ref: ChangeDetectorRef) {
    setInterval(() => {
      this.ref.detectChanges();
    }, 1000);
  }

  startAlgorithm(){
    this.algorithmWeight.stateId = this.app.stateId;
    let normalizedWeight: Weight = Object.assign( Object.create( Object.getPrototypeOf(this.algorithmWeight)), this.algorithmWeight);
    normalizedWeight.normalize();
    this.http.post<Weight>("api/algorithm", normalizedWeight)
      .subscribe(data => {
        /*
         * Post algorithm start, can initialize
         * or setup before polling for changes
         */
        this.app.status = Status.SETUP;
        this.http.post("api/algorithm/start", this.app.user).subscribe();
      },
      error => {
        console.log("Error Log:", error);
      });
  }

  startBatch(){
    this.batchMinWeight.stateId = this.app.stateId;
    let normalizedMinWeight: Weight = Object.assign( Object.create( Object.getPrototypeOf(this.batchMinWeight)), this.batchMinWeight);
    normalizedMinWeight.normalizeMin(this.batchMaxWeight);
    let normalizedMaxWeight: Weight = Object.assign( Object.create( Object.getPrototypeOf(this.batchMaxWeight)), this.batchMaxWeight);
    normalizedMaxWeight.normalizeMax(this.batchMinWeight);
    let arr = [];
    arr.push(normalizedMaxWeight);
    arr.push(normalizedMinWeight);
    this.http.post<Array<Weight>>("api/batch", arr)
      .subscribe(data => {
          /*
           * Post batch start, can set number of iterations
           * to display progress on front end.
           */
          this.http.post("api/batch/start", this.app.user).subscribe();
          this.app.status = Status.BATCH;
          this.app.batchRuns = this.batchMinWeight.iterations;
        },
        error => {
          console.log("Error Log:", error);
        });
  }

  adjustMinSlider(){
    for (let key in this.batchMinWeight) {
      if (this.batchMaxWeight[key] < this.batchMinWeight[key]){
        this.batchMinWeight[key] = this.batchMaxWeight[key];
      }
    }
  }

  adjustMaxSlider(){
    for (let key in this.batchMinWeight) {
      if (this.batchMinWeight[key] > this.batchMaxWeight[key]){
        this.batchMaxWeight[key] = this.batchMinWeight[key];
      }
    }
  }

  authenticated() { return this.app.authenticated; }

}
