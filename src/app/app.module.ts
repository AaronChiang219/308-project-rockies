import { BrowserModule } from '@angular/platform-browser';
import {Injectable, NgModule} from '@angular/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  HTTP_INTERCEPTORS,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpEvent,
  HttpClientModule
} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppService } from './app.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { SidebarComponent } from "./sidebar/sidebar.component";
import { LeafletComponent } from "./leaflet/leaflet.component";

import { AppRoutingModule } from './app-routing.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Observable } from 'rxjs';

import {
  MatToolbarModule, MatSidenavModule,
  MatButtonModule, MatFormFieldModule,
  MatInputModule, MatSliderModule,
  MatDialogModule, MatTabsModule,
  MatSelectModule, MatDividerModule,
  MatMenuModule, MatSlideToggleModule, MatTableModule, MatProgressSpinnerModule,
} from "@angular/material";
import { RegisterDialogComponent } from './register-dialog/register-dialog.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/'},
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent}
];
@Injectable()
export class XhrInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>  {
    return next.handle(req);
  }
}
@NgModule({
  declarations: [
    AppComponent,
    RegisterDialogComponent,
    HomeComponent,
    LoginComponent,
    SidebarComponent,
    LeafletComponent,
  ],
  entryComponents: [  RegisterDialogComponent     
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    LeafletModule.forRoot(),
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule,
    MatDialogModule,
    MatTabsModule,
    MatSelectModule,
    MatDividerModule,
    MatMenuModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MatTableModule,
    MatProgressSpinnerModule,
  ],
  providers: [AppService, {provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {
  
 }

