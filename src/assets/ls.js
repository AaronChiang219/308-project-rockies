var states;
var ctdistricts;
var precincts;
var xhr;
var coDistricts;
var mymap;

function setup(){

  console.log('in setup');

  var container = L.DomUtil.get('mapid');
  console.log(container);
  if(container != null){
    mymap = L.map('mapid').setView([37.09, -95.71], 4);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox.streets'
    }).addTo(mymap);
  }

}

function style(feature) {
	return {
		weight: 2,
		opacity: 1,
		color: 'white',
		dashArray: '3',
		fillOpacity: 0.7,
		fillColor: '#FFEDA0'
	};
}

function highlightAndShowFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    var x = document.getElementById("myDialog");
    x.innerHTML = "<p><strong>ID:</strong>&nbsp;" + e.target.feature.properties.GEOID10 +"</p> " +
    "<p><strong>Name:&nbsp;</strong>&nbsp;" + e.target.feature.properties.NAME10 +"</p> " +
    "<p><strong>Total Population:&nbsp;</strong>" + e.target.feature.properties.TOTAL +"</p> " +
    "<p><strong>Democrat:&nbsp;</strong>" + e.target.feature.properties.REPUBLICAN +"</p> " +
    "<p><strong>Republican:&nbsp;</strong>" + e.target.feature.properties.DEMOCRAT+"</p>";

    console.log(e.target.feature.properties)
}

function resetHighlightAndShow(e) {
    states.resetStyle(e.target);

    var x = document.getElementById("myDialog");
    x.innerText="";
}

function highlightFeature(e) {
  var layer = e.target;

  layer.setStyle({
    weight: 5,
    color: '#666',
    dashArray: '',
    fillOpacity: 0.7
  });

  console.log(e.target.feature.properties)
}

function resetHighlight(e) {
  states.resetStyle(e.target);

}




function addDistricts(e){
    console.log("reached");
    if(xhr.readyState==4 && xhr.status == 200){
        var dts = JSON.parse(xhr.responseText);
        console.log(dts);
        districts = L.geoJson(dts, {
          style: style,
	  onEachFeature: onEachFeature
          }).addTo(mymap);
    }
}

function checkZoom(e){
	console.log("Zoom level");
	console.log(mymap.getZoom());

	console.log('e' + e);

	if(mymap.getZoom()>=8){
		//precincts = L.featureGroup().addTo(mymap);

		if(e==9){
			console.log("CT");
			var ctP = L.geoJson(ctprecincts, {
				style: style,
				onEachFeature: precinctListeners
			}).addTo(mymap);
			

			ctP.eachLayer(function (layer) {
				layer.id=layer.feature.properties.GEOID10
			});

			//mymap.on('moouseover', onMapClick);

			console.log(ctP);
		}

		if(e==8){
			console.log("CO");
			var ctP = L.geoJson(coprecincts, {
				style: style,
				onEachFeature: precinctListeners
			}).addTo(mymap);
			console.log(ctP);
		}
	}

	//mymap.removeLayer(ctDistricts);
}

function getCongressionalDistricts(e){
//     xhr = new XMLHttpRequest();

//     xhr.open('GET', 'http://localhost:8080/api/stateDistricts/09');
//     xhr.send();

//     xhr.onreadystatechange = addDistricts;
	
	if(e==9){
		console.log("looking for ct");
		ctDistricts = L.featureGroup().addTo(mymap);
		var ctD = L.geoJson(ctdistricts, {
			style: style,
			onEachFeature: onEachFeature
		}).addTo(ctDistricts);
	}

	if(e==8){
		console.log("looking for co");
		coDistricts = L.featureGroup().addTo(mymap);
		var ctD = L.geoJson(codistricts, {
			style: style,
			onEachFeature: onEachFeature
		}).addTo(coDistricts);
	}
	
	mymap.removeLayer(states);

	mymap.on('zoomend', function(){
		checkZoom(e);
	});
}

function zoomToFeature(e) {
    console.log("reached");
    mymap.fitBounds(e.target.getBounds());
    var layer = e.target;
    console.log(layer.feature.id);
    getCongressionalDistricts(layer.feature.id);
    
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}

function precinctListeners(feature, layer) {
  layer.on({
    mouseover: highlightAndShowFeature,
    mouseout: resetHighlightAndShow
  });
}

function get(){
  console.log('in');
  setup();
  addStates();
  console.log(mymap);
  return mymap;
}

function addStates(){
  states = L.geoJson(statesData, {
    style: style,
    onEachFeature: onEachFeature
  }).addTo(mymap);
}

