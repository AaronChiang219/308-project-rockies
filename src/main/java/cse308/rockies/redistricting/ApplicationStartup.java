package cse308.rockies.redistricting;

import cse308.rockies.redistricting.algorithm.Algorithm;
import cse308.rockies.redistricting.algorithm.Measures;
import cse308.rockies.redistricting.entities.Precinct;
import cse308.rockies.redistricting.entities.State;
import cse308.rockies.redistricting.entities.Weight;
import cse308.rockies.redistricting.repositories.StateRepository;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.wololo.jts2geojson.GeoJSONReader;
import org.wololo.jts2geojson.GeoJSONWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

  @Autowired
  private StateRepository stateRepository;

  @Override
  public void onApplicationEvent(final ApplicationReadyEvent event){

    Iterable<State> states = stateRepository.findAll();
//    ArrayList<State> states = new ArrayList<>();
//    states.add(stateRepository.findById(9).get());
    StatesIterator statesIterator = StatesIterator.getInstance();
    statesIterator.setStates(states);
    System.out.println("States Loaded");

    Weight weight = new Weight(9, 5);
    //Algorithm al = new Algorithm(weight);

    //al.run();
    //al.runPhaseTwo();

  }
}
