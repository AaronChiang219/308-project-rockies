package cse308.rockies.redistricting.converters;

import java.util.LinkedList;
import java.util.Queue;

public class GUIUpdate {
  private Queue<UpdateList> updates;

  public GUIUpdate(){
    updates = new LinkedList<>();
  }

  public void addToGUIUpdate(UpdateList ul){
    updates.add(ul);
  }

  public Queue<UpdateList> getUpdates() {
    return updates;
  }

  public void setUpdates(Queue<UpdateList> updates) {
    this.updates = updates;
  }
}
