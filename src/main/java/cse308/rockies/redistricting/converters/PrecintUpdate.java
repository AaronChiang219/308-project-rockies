package cse308.rockies.redistricting.converters;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
public class PrecintUpdate {

//  private static final AtomicInteger count = new AtomicInteger(0);
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int aid;
  private String pid;
  private String id;

  public PrecintUpdate(){
    pid="0";
    id="0";
  }

  public PrecintUpdate(String pid, String id){
//    aid=count.incrementAndGet();
    this.pid=pid;
    this.id=id;
  }

  public String getPid() {
    return pid;
  }

  public void setPid(String pid) {
    this.pid = pid;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
