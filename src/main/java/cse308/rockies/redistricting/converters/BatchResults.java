package cse308.rockies.redistricting.converters;

import org.hibernate.engine.jdbc.batch.spi.Batch;

import java.util.ArrayList;

public class BatchResults {
  private ArrayList<BatchSummary> summaries;

  public BatchResults() {
    this.summaries = new ArrayList<>();
  }

  public void addToSummaries(BatchSummary bs){
    summaries.add(bs);
  }

  public ArrayList<BatchSummary> getBatchSummaries(){
    return summaries;
  }
}
