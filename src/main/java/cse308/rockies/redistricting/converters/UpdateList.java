package cse308.rockies.redistricting.converters;

import cse308.rockies.redistricting.algorithm.Measures;
import cse308.rockies.redistricting.entities.State;
import cse308.rockies.redistricting.entities.User;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
public class UpdateList implements Cloneable{
//  @Transient
//  private static final AtomicInteger count = new AtomicInteger(0);
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  @OneToMany
  private List<PrecintUpdate> updates;
  private int status;

  @ManyToOne
  private User user;

  private double compactnessPolsbyPopper;
  private double compactnessSchwartzberg;
  private double equalPopulation;
  private double efficiencyGap;
  private double objectiveFunction;

  public UpdateList(){
//    id=count.incrementAndGet();
    updates = new ArrayList<>();
    status=0;
    user = new User();

    compactnessPolsbyPopper=0;
    compactnessSchwartzberg=0;
    equalPopulation=0;
    efficiencyGap=0;
    objectiveFunction = 0;
  }

  public UpdateList(User user){
    updates = new ArrayList<>();
    status=0;
    this.user=user;

    compactnessPolsbyPopper=0;
    compactnessSchwartzberg=0;
    equalPopulation=0;
    efficiencyGap=0;
    objectiveFunction = 0;
  }

  public void addToUpdates(PrecintUpdate pu){
    updates.add(pu);
  }

  public void updateStateInfo(State s){
    compactnessPolsbyPopper = Measures.statePolsbyPopper(s);
    compactnessSchwartzberg = Measures.stateSchwartzberg(s);
    equalPopulation = Measures.stateEqualPopulation(s);
    efficiencyGap = Measures.stateEfficiencyGap(s);
    objectiveFunction = Measures.stateScore(s);
  }

  public UpdateList clone() throws CloneNotSupportedException {
    UpdateList clone = (UpdateList)super.clone();
    clone.updates = new ArrayList<>(updates);
    return clone;
  }

  public void clearUpdates(){
    updates.clear();
  }

  public int getId() {
    return id;
  }

  public void setId(int id){
    this.id=id;
  }

  public List<PrecintUpdate> getUpdates() {
    return updates;
  }

  public void setUpdates(List<PrecintUpdate> updates) {
    this.updates = updates;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public double getCompactnessPolsbyPopper() {
    return compactnessPolsbyPopper;
  }

  public void setCompactnessPolsbyPopper(double compactnessPolsbyPopper) {
    this.compactnessPolsbyPopper = compactnessPolsbyPopper;
  }

  public double getCompactnessSchwartzberg() {
    return compactnessSchwartzberg;
  }

  public void setCompactnessSchwartzberg(double compactnessSchwartzberg) {
    this.compactnessSchwartzberg = compactnessSchwartzberg;
  }

  public double getEqualPopulation() {
    return equalPopulation;
  }

  public void setEqualPopulation(double equalPopulation) {
    this.equalPopulation = equalPopulation;
  }

  public double getEfficiencyGap() {
    return efficiencyGap;
  }

  public void setEfficiencyGap(double efficiencyGap) {
    this.efficiencyGap = efficiencyGap;
  }

  public double getObjectiveFunction() {
    return objectiveFunction;
  }

  public void setObjectiveFunction(double objectiveFunction) {
    this.objectiveFunction = objectiveFunction;
  }

}
