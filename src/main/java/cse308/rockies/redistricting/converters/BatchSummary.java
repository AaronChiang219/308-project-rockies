package cse308.rockies.redistricting.converters;

import java.util.List;
import java.util.ArrayList;

public class BatchSummary {

  private int run;
  private int mapId;
  private int democratic;
  private int republican;
  private double objectiveFunctionValues;
  //private double compactness;
  private double compactnessPolsbyPoppper;
  private double compactnessSchwartzberg;
  private double equalPopulation;
  private double efficiencyGap;
  private List<Double> meanMedianDifference;

  public BatchSummary(int run, int democratic, int republican, int mapid){
    this.run=run;
    this.democratic=democratic;
    this.republican=republican;
    this.mapId=mapid;
    objectiveFunctionValues=0;
    //compactness=0;
    compactnessPolsbyPoppper=0;
    compactnessSchwartzberg=0;
    equalPopulation=0;
    efficiencyGap=0;
    meanMedianDifference=new ArrayList<>();
  }

  public double getObjectiveFunctionValues() {
    return objectiveFunctionValues;
  }

  //public double getCompactness() {
  //  return compactness;
  //}

  public int getDemocratic() {
    return democratic;
  }

  public int getRun() {
    return run;
  }

  public int getRepublican() {
    return republican;
  }

  //public void setCompactness(double compactness) {
  //  this.compactness = compactness;
  //}

  public void setRun(int run) {
    this.run = run;
  }

  public void setDemocratic(int democratic) {
    this.democratic = democratic;
  }

  public void setObjectiveFunctionValues(double objectiveFunctionValues) {
    this.objectiveFunctionValues = objectiveFunctionValues;
  }

  public void setRepublican(int republican) {
    this.republican = republican;
  }

  public double getCompactnessPolsbyPoppper() {
    return compactnessPolsbyPoppper;
  }

  public void setCompactnessPolsbyPoppper(double compactnessPolsbyPoppper) {
    this.compactnessPolsbyPoppper = compactnessPolsbyPoppper;
  }

  public double getCompactnessSchwartzberg() {
    return compactnessSchwartzberg;
  }

  public void setCompactnessSchwartzberg(double compactnessSchwartzberg) {
    this.compactnessSchwartzberg = compactnessSchwartzberg;
  }

  public double getEqualPopulation() {
    return equalPopulation;
  }

  public void setEqualPopulation(double newValue) {
    this.equalPopulation = newValue;
  }

  public double getEfficiencyGap() {
    return efficiencyGap;
  }

  public void setEfficiencyGap(double newValue) {
    this.efficiencyGap = newValue;
  }


  public List<Double> getMeanMedianDifference() {
    return meanMedianDifference;
  }

  public void setMeanMedianDifference(List<Double> meanMedianDifference) {
    this.meanMedianDifference = meanMedianDifference;
  }

  public int getMapId() {
    return mapId;
  }

  public void setMapId(int mapId) {
    this.mapId = mapId;
  }

}
