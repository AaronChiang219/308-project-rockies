package cse308.rockies.redistricting.repositories;

import cse308.rockies.redistricting.entities.State;
import org.springframework.data.repository.CrudRepository;


public interface StateRepository extends CrudRepository<State, Integer> {

}
