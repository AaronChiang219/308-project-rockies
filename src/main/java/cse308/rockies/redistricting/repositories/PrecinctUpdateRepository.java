package cse308.rockies.redistricting.repositories;

import cse308.rockies.redistricting.converters.PrecintUpdate;
import org.springframework.data.repository.CrudRepository;

public interface PrecinctUpdateRepository extends CrudRepository<PrecintUpdate, Integer> {

}
