package cse308.rockies.redistricting.repositories;

import cse308.rockies.redistricting.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
  User findByUsername(String username);
}
