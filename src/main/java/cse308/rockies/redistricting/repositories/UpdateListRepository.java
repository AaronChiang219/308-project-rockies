package cse308.rockies.redistricting.repositories;

import cse308.rockies.redistricting.converters.UpdateList;
import cse308.rockies.redistricting.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface UpdateListRepository extends CrudRepository<UpdateList, Integer> {
  Set<UpdateList> findAllByUser(User user);

}
