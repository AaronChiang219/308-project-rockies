package cse308.rockies.redistricting.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class VoterInfo {

  @Id
  private String id;

  private int votingPopulation;

  private int democrats;

  private int republicans;

  public VoterInfo(){

  }

  public VoterInfo(VoterInfo voterInfo){
    this.id="C" + voterInfo.id;
    this.votingPopulation = voterInfo.votingPopulation;
    this.democrats = voterInfo.democrats;
    this.republicans = voterInfo.republicans;
  }

  public void merge(VoterInfo voterInfo){
    this.votingPopulation+=voterInfo.votingPopulation;
    this.democrats+=voterInfo.democrats;
    this.republicans+=voterInfo.republicans;
  }

  public void remove(VoterInfo voterInfo){
    this.votingPopulation-=voterInfo.votingPopulation;
    this.democrats-=voterInfo.democrats;
    this.republicans-=voterInfo.republicans;
  }

  public int getVotingPopulation() {
    return votingPopulation;
  }
  
  public int getDemocrats() {
	  return democrats;
  }
  
  public int getRepublicans() {
	  return republicans;
  }

  public void setDemocrats(int democrats) {
    this.democrats = democrats;
  }

  public void setRepublicans(int republicans) {
    this.republicans = republicans;
  }
}
