package cse308.rockies.redistricting.entities;

import cse308.rockies.redistricting.algorithm.Measures;
import cse308.rockies.redistricting.algorithm.Merge;
import cse308.rockies.redistricting.converters.BatchSummary;
import cse308.rockies.redistricting.converters.PrecintUpdate;
import cse308.rockies.redistricting.converters.UpdateList;

import javax.persistence.*;
import java.util.*;

@Entity
public class State implements Cloneable{

  @Id
  private Integer id;
  private String name;
  private int population;

  @OneToMany(fetch=FetchType.EAGER)
  private Set<Precinct> precincts;

//  @OneToMany(fetch=FetchType.EAGER)
  @Transient
  private Set<District> districts;

  @OneToMany(fetch=FetchType.EAGER)
  private Set<Cluster> clusters;

  @Transient
  private Set<Merge> merges;

  @Transient
  private Map<Cluster, ClusterEdge> edges;

  @OneToOne
  private VoterInfo voterInfo;

  public State(){

    merges = new HashSet<>();
    edges = new HashMap<>();
    districts = new HashSet<>();
  }

  @PostLoad
  public void init(){
    for (Cluster c: clusters
         ) {
      c.initMap();

    }

    //todo: remove later
    for (Precinct p : precincts
         ) {
      if(p.getVoterInfo().getRepublicans()>p.getPopulation() || p.getVoterInfo().getDemocrats()>p.getPopulation()){
        System.out.println("Messing up");
      }
    }

  }

  public int getId(){
    return this.id;
  }

  public State clone() throws CloneNotSupportedException {
    State clone = (State)super.clone();
    clone.precincts=new HashSet<>(precincts);
    clone.districts=new HashSet<>();
    clone.clusters=new HashSet<>();

    Map<Cluster, Set<Cluster>> neighborsMapping = new HashMap<>();
    Map<Cluster, Cluster> clusterMapping = new HashMap<>();

    for (Cluster c: clusters
         ) {
      Cluster cc = c.clone();
      clone.clusters.add(cc);

      neighborsMapping.put(cc, c.getNeighbors());
      clusterMapping.put(c, cc);
    }

    for (Map.Entry<Cluster, Set<Cluster>> neighborSet: neighborsMapping.entrySet()
         ) {
      Set<Cluster> newNeighbors = new HashSet<>();

      for (Cluster c: neighborSet.getValue()
           ) {
        newNeighbors.add(clusterMapping.get(c));
      }

      neighborSet.getKey().setNeighbors(newNeighbors);
    }

    for (Cluster c: clone.clusters
         ) {
      c.init();
      c.initMap();
    }

    return clone;
  }

  public State cloneWithDistricts() throws CloneNotSupportedException{
    State clone = (State) super.clone();
    clone.districts=new HashSet<>(districts);
    return clone;
  }

  public String getName() {
    return name;
  }

  public Set<Precinct> getPrecincts() {
    return precincts;
  }

  public Set<District> getDistricts() {
    return districts;
  }

  public Set<Cluster> getClusters() {
    return clusters;
  }

  public Set<Merge> getMerges() { return merges; }

  public void addToMerges(Merge merge){
    merges.add(merge);
  }
  
  public BatchSummary getSummary(int run, int databaseId) {
    int republicanDistricts=0;
    int democraticDistricts=0;

    for (District d: districts
         ) {
      if(d.getVoterInfo().getDemocrats()>d.getVoterInfo().getRepublicans())
        democraticDistricts++;
      else
        republicanDistricts++;
    }
    BatchSummary summary = new BatchSummary(run, democraticDistricts, republicanDistricts, databaseId);
    summary.setObjectiveFunctionValues(Measures.stateScore(this));
    summary.setCompactnessPolsbyPoppper(Measures.statePolsbyPopper(this));
    summary.setCompactnessSchwartzberg(Measures.stateSchwartzberg(this));
    summary.setEqualPopulation(Measures.stateEqualPopulation(this));
    summary.setEfficiencyGap(Measures.stateEfficiencyGap(this));
    summary.setMeanMedianDifference(Measures.meanMedianDifference(this));
    return summary;
  }

  public VoterInfo getVoterInfo() {
    return voterInfo;
  }

  public int getPopulation() {
    return population;
  }

  public District pickRandomDistrict(){
    int chosen = new Random().nextInt(districts.size());
    int i = 0;

    for(District d: districts){
      if (i==chosen)
        return d;
      i++;
    }

    return null;
  }

  public Cluster pickRandomCluster(){
    int chosen = new Random().nextInt(clusters.size());
    int i = 0;

    for(Cluster c: clusters){
      if (i==chosen)
        return c;
    }

    return null;
  }

  public double getOverallValue(){
    //TODO: implement actual overall value
    return 1.0;
  }

  public Cluster getMostAttractiveNeighbor(Cluster c){
    boolean inClusters;
    ClusterEdge ce;
    Cluster b;
    do{

      ce = c.getMostAttractiveNeighbor();

      if(ce==null)
        return null;

      b = ce.getB();

      inClusters = clusters.contains(b);

      if(inClusters==false)
        c.getEdges().remove(ce);
    }while(inClusters==false);

    return b;

  }


  public int determineIdealPopulation() {
    return population/clusters.size();
  }

  public void convertClustersToDistricts(Weight weight) {
    HashMap<District, Set<Cluster>> districtClusters = new HashMap<>();
    for (Cluster c: clusters
    ) {
      District district = new District(c);
      districts.add(district);
      c.setDistrict(district);
      districtClusters.put(district, c.getNeighbors());
    }

    for (Map.Entry<District, Set<Cluster>> entry: districtClusters.entrySet()
    ) {
      District d = entry.getKey();
      Set<Cluster> neighbors = entry.getValue();

      for (Cluster c: neighbors
      ) {
        d.addToNeighobors(c);
      }

      d.initBorderPrecincts();
    }

    for (District d: districts
    ) {
      d.setScore(Measures.getNormalizedScore(d, this, weight));
    }
  }




  public void replaceDistricts(District d, District newD) {
//    District districtToRemove=null;
//    for (District district: districts
//         ) {
//      if(district.getId()==d.getId()){
//        districtToRemove=district;
//        break;
//      }
//    }

    districts.remove(d);
    districts.add(newD);
  }

  public void pushDistrictUpdate(UpdateList u, PrecintUpdate pu) {
    u.addToUpdates(pu);
    u.updateStateInfo(this);
  }

  public void pushUpdate(UpdateList u, int iteration) {

    if (u.getId() < iteration) {
      for (Cluster c: clusters){
        for (Precinct p: c.getPrecincts()
        ) {
          PrecintUpdate pu = new PrecintUpdate(p.getId(), c.getId());
          u.addToUpdates(pu);
        }
      }
      u.setId(iteration);
//      u.updateStateInfo(this);
    }


  }

  public District getDistrictWithLowestScore(){
    District lowest = null;

    for (District d: districts
    ) {
//      if(lowest==null || lowest.getPopulation()>d.getPopulation()){
      if(lowest==null || lowest.getScore()<d.getScore()){
        lowest=d;
      }
    }
    return lowest;
  }

  public boolean areDistrictsContiguous(){

    for (District d: districts
    ) {
      if(!d.reachesAllNeighbors())
        return false;
    }

    return true;
  }

  public void resetVisitedPrecincts() {
    for (Precinct p: precincts
    ) {
      p.setVisited(false);
    }
  }

  public UpdateList pushFinalValues(User user) {
    UpdateList ul = new UpdateList(user);

    for (District d: districts ) {
      String id = d.getId();
      for (Precinct p : d.getPrecincts() ) {
        PrecintUpdate pu = new PrecintUpdate(p.getId(), id);
        ul.addToUpdates(pu);
      }
    }
    ul.updateStateInfo(this);
    return ul;
  }
}
