package cse308.rockies.redistricting.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
public class ClusterEdge implements Comparable<ClusterEdge>{

  @Id
  private Integer id;
  private static final AtomicInteger count = new AtomicInteger(1);

  @OneToOne
  private Cluster a;

  @OneToOne
  private Cluster b;
  private double countyAttraction;

  public ClusterEdge(Cluster a, Cluster b, boolean sameCounty){
    id=count.incrementAndGet();
    this.a=a;
    this.b=b;
    if(sameCounty)
      countyAttraction=0.9;
    else
      countyAttraction=0.1;
  }

  public double getCountyAttraction() {
    return countyAttraction;
  }

  public Cluster getA() { return a; }

  public Cluster getB() { return b; }

  public void setA(Cluster a) {
    this.a = a;
  }

  public void setB(Cluster b) {
    this.b = b;
  }

  public void setCountyAttraction(double county) {
    this.countyAttraction = county;
  }

  @Override
  public int compareTo(ClusterEdge o) {
//    System.out.println("Been here instead");
//    if(this.id==o.id || (this.a == o.getA() && this.b==o.getB()))
    if(this.a == o.getA() && this.b==o.getB())
      return 0;
    else{
      if(this.getCountyAttraction()<o.getCountyAttraction())
        return 1;
//    else if(this.getCounty()>o.getCounty())
//      return -1;
//    return 0;
      return -1;
    }

  }

  public boolean equals(Object o){
//    System.out.println("Been here");
    if(o==this)
      return true;

    if(!(o instanceof ClusterEdge))
      return false;

    ClusterEdge c = (ClusterEdge) o;

    return c.getA()==((ClusterEdge) o).getA() && c.getB()==((ClusterEdge) o).getB();
  }

  public String toString(){
    return "A: " + a.getId() + "; B: " + b.getId();
  }
}
