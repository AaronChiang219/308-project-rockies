/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cse308.rockies.redistricting.entities;

/**
 *
 * @author Zijian Liu
 */
public enum Parties {
	REPUBLICAN, DEMOCRATIC, OTHER;
}
