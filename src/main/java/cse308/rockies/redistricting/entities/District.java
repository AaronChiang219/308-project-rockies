package cse308.rockies.redistricting.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.GeometryFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class District {
  private static final AtomicInteger count = new AtomicInteger(1);

  private String id;
  private int population;
  private VoterInfo voterInfo;
  private Geometry geometry;
  private Set<Precinct> precincts;
  private Set<District> neighbors;
  private Set<Precinct> borderPrecincts;
  private double score;

  public District(District d, Precinct p){
    id=d.id;
    population=d.population+p.getPopulation();
    score=0;

    voterInfo=d.voterInfo;

    //Combine Geometry
    List<Geometry> geometries = new ArrayList<>();
    geometries.add(d.geometry);
    geometries.add(p.getShape());
    GeometryFactory factory = new GeometryFactory();
    GeometryCollection combinedGeo = (GeometryCollection)factory.buildGeometry(geometries);
    geometry = combinedGeo.union();

    precincts = new HashSet<>(d.precincts);
    precincts.add(p);

    neighbors = d.neighbors;

    borderPrecincts = new HashSet<>();
    initBorderPrecincts();
  }

  public District(Cluster c) {
    id=c.getId();
    population=c.getPopulation();
    voterInfo=c.getVoterInfo();
    geometry=c.getShape();
    precincts=c.getPrecincts();
    neighbors=new HashSet<>();
    borderPrecincts=new HashSet<>();
    score=0;

    for (Precinct p: precincts
         ) {
      p.setDistrict(this);
    }
  }

  public void addToNeighobors(Cluster c){
    neighbors.add(c.getDistrict());
  }

  public int getPopulation() {
	  return population;
  }
  
  public Geometry getGeometry() {
	  return geometry;
  }
  
  public VoterInfo getVoterInfo() {
	  return voterInfo;
  }

  public Precinct getABorderPrecinct(){
    int chosen = new Random().nextInt(borderPrecincts.size());
    int i = 0;

    for(Precinct p: borderPrecincts){
      if (i==chosen){
        return p.getNeighborInDifferentDistrict();
      }
      i++;
    }

    return null;
  }

  public void initBorderPrecincts() {
    for (Precinct p: precincts
         ) {
      if(p.allNeighborsInSameDistrict()==false)
        borderPrecincts.add(p);
    }
  }

  public String getId() {
    return id;
  }

  public boolean equals(Object o){
    if (o == this) {
      return true;
    }

    if (!(o instanceof District)) {
      return false;
    }

    District d = (District) o;

    return d.id==this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setPopulation(int population) {
    this.population = population;
  }

  public void setVoterInfo(VoterInfo voterInfo) {
    this.voterInfo = voterInfo;
  }

  public void setGeometry(Geometry geometry) {
    this.geometry = geometry;
  }

  public Set<Precinct> getPrecincts() {
    return precincts;
  }

  public void setPrecincts(Set<Precinct> precincts) {
    this.precincts = precincts;
  }

  public Set<District> getNeighbors() {
    return neighbors;
  }

  public void setNeighbors(Set<District> neighbors) {
    this.neighbors = neighbors;
  }

  public Set<Precinct> getBorderPrecincts() {
    return borderPrecincts;
  }

  public void setBorderPrecincts(Set<Precinct> borderPrecincts) {
    this.borderPrecincts = borderPrecincts;
  }

  public void removePrecinct(Precinct p){
    population=population-p.getPopulation();
    //todo: remove voterdata
//    this.voterInfo.remove(p.getVoterInfo());

    int newDems = this.getVoterInfo().getDemocrats() - p.getVoterInfo().getDemocrats();
    int newRep = this.getVoterInfo().getRepublicans() - p.getVoterInfo().getRepublicans();

    this.voterInfo.setDemocrats(newDems);
    this.voterInfo.setRepublicans(newRep);

    geometry=geometry.difference(p.getShape());
    precincts.remove(p);

    initBorderPrecincts();
  }

  public void addPrecinct(Precinct p){
    population=population+p.getPopulation();
    //todo: add voterdata
//    this.voterInfo.merge(p.getVoterInfo());

    int newDems = this.getVoterInfo().getDemocrats() + p.getVoterInfo().getDemocrats();
    int newRep = this.getVoterInfo().getRepublicans() + p.getVoterInfo().getRepublicans();

    this.voterInfo.setDemocrats(newDems);
    this.voterInfo.setRepublicans(newRep);

    geometry=geometry.union(p.getShape());
    precincts.add(p);

    p.setDistrict(this);

    initBorderPrecincts();
  }

  public double getScore() {
    return score;
  }

  public void setScore(double score) {
    this.score = score;
  }

  public boolean reachesAllNeighbors() {

    Precinct chosen = null;

    for (Precinct p: precincts
         ) {
      chosen=p;
      break;
    }

    int traversablePrecincts = dfsUsingPrecincts(chosen);

    return traversablePrecincts==precincts.size();
  }

  private int dfsUsingPrecincts(Precinct p) {
    int total = 0;
    Stack<Precinct> stack = new Stack<>();
    stack.add(p);
    total++;
    p.setVisited(true);

    while(!stack.isEmpty()){
      Precinct precinct = stack.pop();

      for (Precinct n: precinct.getNeighbors()
           ) {
        if(n!=null && !n.isVisited() && n.getDistrict()==this){
          stack.add(n);
          total++;
          n.setVisited(true);
        }
      }
    }

    System.out.println("Visited : " + total);
    return total;
  }
}
