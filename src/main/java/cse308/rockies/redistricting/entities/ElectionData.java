package cse308.rockies.redistricting.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ElectionData {

  @Id
  private Integer id;

  private int year;

  private int totalVotes;

  private int democratVotes;

  private int republicanVotes;

  private int otherVotes;
  
  public int getTotalVotes() {
	  return totalVotes;
  }
   public int getDemocratVotes() {
	  return democratVotes;
  }
    public int getRepublicanVotes() {
	  return republicanVotes;
  }
	 public int getOtherVotes() {
	  return otherVotes;
  }
}