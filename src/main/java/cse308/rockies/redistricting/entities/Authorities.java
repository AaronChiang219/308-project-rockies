package cse308.rockies.redistricting.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "AUTHORITIES")
public class Authorities {
  @Id
  @Column(name = "AUTHORITY")
  private String authority;

  @ManyToOne
  @JoinColumn(name = "USERNAME")
  private User user;

  public String getAuthority() {
    return authority;
  }

  public User getUser() {
    return user;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
