package cse308.rockies.redistricting.entities;

import cse308.rockies.redistricting.converters.GeometryConverter;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.*;
import java.util.*;

@Entity
public class Cluster implements Comparable<Cluster>, Cloneable{

  @Id
  private String id;

  private int population;

  @Transient
  private int county;

//  @OneToOne
  @Transient
  private VoterInfo voterInfo;

  @Transient
  private boolean usedInIteration;

  @OneToMany(fetch= FetchType.EAGER)
  private Set<Precinct> precincts;

  @OneToMany(fetch=FetchType.EAGER)
//  @Transient
  private Set<Cluster> neighbors;

//  @OneToMany(fetch=FetchType.EAGER)
//  @OrderBy("countyounty")
  @Transient
  private SortedSet<ClusterEdge> edges;

  @Column(columnDefinition = "JSON")
  @Convert(converter = GeometryConverter.class)
  public Geometry shape;

  @Transient
  private Map<Cluster, ClusterEdge> edgeMap;

  @Transient
  private District district;

  public Cluster(){
    usedInIteration=false;
    edgeMap=new HashMap<>();
    edges=new TreeSet<>();
//    neighbors=new HashSet<>();
  }

  @PostLoad
  public void init(){
    for (Precinct p: precincts
         ){
      county = p.getCounty();
      shape = p.getShape().copy();
      voterInfo = new VoterInfo(p.getVoterInfo());
    }

  }

  public Cluster clone() throws CloneNotSupportedException{
    Cluster clone = (Cluster) super.clone();

    clone.precincts = new HashSet<>(precincts);
    clone.neighbors = new HashSet<>();
    clone.edges = new TreeSet<>();
    clone.edgeMap = new HashMap<>();

    return clone;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public boolean isUsedInIteration() {
    return usedInIteration;
  }

  public void setUsedInIteration(boolean usedInIteration) {
    this.usedInIteration = usedInIteration;
  }

  public int getPopulation() {
    return population;
  }

  public VoterInfo getVoterInfo(){
    return voterInfo;
  }

  public void setPopulation(int population) {
    this.population = population;
  }

  public Set<Precinct> getPrecincts() {
    return precincts;
  }

  public void setPrecincts(Set<Precinct> precincts) {
    this.precincts = precincts;
  }

  public Set<Cluster> getNeighbors() {
    return neighbors;
  }

  public void setNeighbors(Set<Cluster> neighbors) {
    this.neighbors = neighbors;
  }

  public SortedSet<ClusterEdge> getEdges() {
    return edges;
  }

  public void setEdges(SortedSet<ClusterEdge> edges) {
    this.edges = edges;
  }

  public Geometry getShape() {
    return shape;
  }

  public void setShape(Geometry shape) {
    this.shape = shape;
  }

  public Map<Cluster,ClusterEdge> getEdgeMap(){ return edgeMap; }

  public District getDistrict(){
    return district;
  }

  public void setDistrict(District district){
    this.district=district;
  }

  public ClusterEdge getMostAttractiveNeighbor(){

    if(edges.size()>0)
      return edges.first();
    else
      return null;
  }

  public ClusterEdge getCommonEdge(Cluster clusterB){
    for (ClusterEdge ce: edges) {
      if(ce.getB().equals(clusterB))
        return ce;
    }
    return null;
  }

  public void redirectEdges(Cluster clusterB){
    ClusterEdge edgeToRemove = null;
    for (ClusterEdge ce: edges) {
      if(ce.getB()==clusterB)
        edgeToRemove=ce;
      else
        ce.setA(clusterB);
    }

    if(edgeToRemove!=null)
      edges.remove(edgeToRemove);
    return;
  }

  public void redirectPrecincts(Cluster clusterB){
    for (Precinct p: precincts) {
      p.setCluster(clusterB);
    }
    return;
  }

  public List<Precinct> getBorderPrecincts(){
    List<Precinct> borderPrecincts = new ArrayList<>();
    for(Precinct p: precincts){
      for(Precinct n: p.getNeighbors()){
        if(p.getCluster()==n.getCluster()){
          borderPrecincts.add(p);
          break;
        }
      }
    }
    return borderPrecincts;
  }

//  public void mergeClusterEdges(Cluster b){
//    if(this!=b)
//      this.edges.addAll(b.getEdges());
//
////    for (ClusterEdge ce: b.getEdges()
////         ) {
////      this.edges.add(ce);
////      System.out.println(ce);
////    }
//  }

  public boolean equals(Object o){
    System.out.println("Been here");
    if(o==this)
      return true;

    if(!(o instanceof Cluster))
      return false;

    Cluster c = (Cluster) o;

    return c.getId()==this.getId();


  }

  public void removeFromNeighbors(Cluster a){
    neighbors.remove(a);
  }

  public void removeFromEdges(ClusterEdge ce){
    edges.remove(ce);
  }

  @Override
  public int compareTo(Cluster o) {
    if(this.getId()==o.getId())
      return 0;
    return -1;
  }

  public ClusterEdge removeEdgeForCluster(Cluster clusterB) {
    for (ClusterEdge ce: edges
         ) {
      if(ce.getB()==clusterB)
        return ce;
    }
    return null;
  }

  public void mergeClusterEdges(Cluster clusterB){
    SortedSet<ClusterEdge> bEdges = clusterB.getEdges();

    SortedSet<ClusterEdge> newEdges = new TreeSet<>();

    System.out.println(edges);
    System.out.println(bEdges);

//    ClusterEdge edgeToRemove = null;
//
//    for (ClusterEdge ce: bEdges
//         ) {
//      Cluster b = ce.getB();
//      if(b!=this){
//        if(neighbors.contains(b)){
//          ce.setCountyAttraction(ce.getCountyAttraction()*2);
//        }
//        ce.setA(this);
//      }else{
//        edgeToRemove=ce;
//      }
//    }
//
//    if(edgeToRemove!=null)
//      bEdges.remove(edgeToRemove);
//
//    Set<ClusterEdge> edgesToRemove = new HashSet<>();

    for (ClusterEdge ce: bEdges
         ) {
      Cluster b = ce.getB();
      if(b!=this){
        if(neighbors.contains(b)){
          ce.setCountyAttraction(ce.getCountyAttraction()*2);
        }
        ce.setA(this);
        newEdges.add(ce);

        ClusterEdge neighbor = ce.getB().getEdgeMap().get(clusterB);

        if(neighbor!=null){
          int sizeBefore = b.edgeMap.size();
          int sizeBeforeN = b.neighbors.size();
          int a = b.edges.size();

          System.out.println("Init Sizes:" + sizeBefore + "; " + sizeBeforeN);

          if(b.getNeighbors().size()!=b.getEdges().size())//|| clusterB.getNeighbors().size()!=clusterB.getEdgeMap().size())
            System.out.println("Not equal");

          System.out.println(b.neighbors);
          System.out.println(b.edgeMap);


          b.edgeMap.remove(clusterB);
          b.neighbors.remove(clusterB);

          System.out.println(b.neighbors);
          System.out.println(b.edgeMap);

          if(b.neighbors.contains(this)){

//            b.removeEdge(neighbor);

            SortedSet<ClusterEdge> nEdges = new TreeSet<>();


            for (ClusterEdge ee: b.getEdges()
                 ) {
              if(ee.getB()!=clusterB)
                nEdges.add(ee);
            }

            b.edges=nEdges;

            System.out.println("if part");
            System.out.println(b.neighbors);
            System.out.println(b.edgeMap);
          }else{
            neighbor.setB(this);
            b.edgeMap.put(this, neighbor);
            b.neighbors.add(this);

            System.out.println("else part");
            System.out.println(b.neighbors);
            System.out.println(b.edgeMap);
          }







          int sizeAfter = b.edgeMap.size() ;
          int sizeAfterN = b.neighbors.size();

          System.out.println("Final Sizes:" + sizeAfter + "; " + sizeAfterN);

          if(b.getNeighbors().size()!=b.getEdges().size())//|| clusterB.getNeighbors().size()!=clusterB.getEdgeMap().size())
            System.out.println("Not equal");

        }

      }
    }


    for (ClusterEdge ce: edges
    ) {
      Cluster b = ce.getB();
      if(b!=clusterB && !(clusterB.getNeighbors().contains(b))){
        newEdges.add(ce);
      }
    }

//    for (ClusterEdge ce: edges
//    ) {
//      Cluster b = ce.getB();
//      if(b==clusterB){
//        edgesToRemove.add(ce);
//      }else if(clusterB.getNeighbors().contains(b)){
//        edgesToRemove.add(ce);
//      }
//    }

//    edges.removeAll(edgesToRemove);



//    for(ClusterEdge ce: edgesToRemove){
//      edges.remove(ce);
//    }
//
//    edges.addAll(bEdges);

    edgeMap.clear();
    edges=newEdges;
    buildMap();

    System.out.println(edges);

//    initMap();

    System.out.println("Edges should be good!");


    Set<Cluster> clusterANeighbors = this.getNeighbors();
    Set<Cluster> clusterBNeighbors = clusterB.getNeighbors();

    System.out.println(clusterANeighbors);
    System.out.println(clusterBNeighbors);


    clusterANeighbors.addAll(clusterBNeighbors);
    clusterANeighbors.remove(this);
    clusterANeighbors.remove(clusterB);

    if(this.getNeighbors().size()!=this.getEdges().size())// || clusterA.getNeighbors().size()!=clusterA.getEdgeMap().size())
      System.out.println("Not equal");

  }

  private void buildMap() {
    for (ClusterEdge ce: edges
         ) {
      edgeMap.put(ce.getB(), ce);
    }
  }

  public String toString(){
    return "Cluster ID: " + id;
  }


  public void removeNeighbor(Cluster neighbor) {


  }

  public void removeEdge(ClusterEdge ce){
    edges.remove(ce);
  }

  public void initMap() {
//    for (ClusterEdge ce: edges
//         ) {
//      edgeMap.put(ce.getB(), ce);
////      neighbors.add(ce.getB());
//    }


    for(Cluster c: neighbors){
      boolean sameCounty = this.county == c.county;

      ClusterEdge ce = new ClusterEdge(this, c, sameCounty);
      edges.add(ce);
      edgeMap.put(c, ce);
    }
  }

  public Cluster getANeighbor(){
    System.out.println(neighbors.size());

    if(neighbors.size()<=0)
      return null;
    int item = new Random().nextInt(neighbors.size());
    int i = 0;

    for(Cluster c: neighbors){
      if(i==item)
        return c;
      i++;
    }
    return null;
  }
}
