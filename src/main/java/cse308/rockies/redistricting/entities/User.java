package cse308.rockies.redistricting.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USERS")
public class User {
  @Id
  @Column(name = "USERNAME")
  private String username;

  @Column(name = "PASSWORD", nullable = false)
  private String password;

  @Column(name = "ENABLED", nullable = false)
  private boolean enabled;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
  private Set<Authorities> authorities = new HashSet<>();

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public Set<Authorities> getAuthorities() {
    return authorities;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setAuthorities(Set<Authorities> authorities) {
    this.authorities = authorities;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setUsername(String username) {
    this.username = username;
  }
}

