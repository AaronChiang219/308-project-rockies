package cse308.rockies.redistricting.entities;

import javax.persistence.*;
import java.util.Set;

import cse308.rockies.redistricting.converters.GeometryConverter;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;

@Entity
public class Precinct {

  @Id
  private String id;

  private int county;
  private int population;

  @Transient
  private boolean visited;

  @OneToOne
  private VoterInfo voterInfo;

  @OneToMany
  private Set<ElectionData> electionData;

  @OneToOne
  private Cluster cluster;

  @Transient
  private District district;

  @Column(columnDefinition = "JSON")
  @Convert(converter = GeometryConverter.class)
  private Geometry shape;

  @OneToMany(fetch= FetchType.EAGER)
  private Set<Precinct> neighbors;

  public Precinct(){ visited=false; }

  public String getId() {
    return id;
  }

  public int getPopulation(){
    return population;
  }

  public Geometry getShape(){ return shape; }

  public Cluster getCluster() {
    return cluster;
  }

  public void setCluster(Cluster cluster) {
    this.cluster = cluster;
  }

  public void setDistrict(District district){
    this.district=district;
  }

  public Set<Precinct> getNeighbors(){
    return neighbors;
  }

  public int getCounty() { return county; }

  public VoterInfo getVoterInfo(){ return voterInfo; }

  public boolean allNeighborsInSameDistrict() {
    for (Precinct p: neighbors
         ) {
      if(p.district!=this.district)
        return false;
    }
    return true;
  }

  public Precinct getNeighborInDifferentDistrict() {
    for (Precinct p: neighbors
         ) {
      if(p.district.getId()!=district.getId()){
        return p;
      }
    }

    return null;
  }

  public District getDistrict() {
    return district;
  }

  public void setVisited(boolean b) {
    visited=b;
  }

  public boolean isVisited() {
    return visited;
  }

//  public boolean equals(Object o){
//    if (o == this) {
//      return true;
//    }
//
//    if (!(o instanceof Precinct)) {
//      return false;
//    }
//
//    Precinct p = (Precinct) o;
//
//    return p.id==this.id;
//  }
}
