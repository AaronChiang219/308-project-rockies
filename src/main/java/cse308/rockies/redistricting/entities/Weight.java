package cse308.rockies.redistricting.entities;

public class Weight {
  private int stateId;
  private int desiredDistricts;
  private int iterations;
  private double compactness;
  private double schwartzberg;
  private double polsbyPopper;
  private double seatsToVotes;
  private double efficiencyGap;
  private double meanMedianDifference;
  private double equalPopulation;

  public Weight(int stateId, int iterations) {
	  this.stateId = stateId;
	  this.iterations = iterations;
	  desiredDistricts=5;

	  compactness=0.0;
	  efficiencyGap=0.0;
	  meanMedianDifference=0.0;
	  equalPopulation=1.0;
  }

  public double getCompactness() {
    return compactness;
  }

  public double getEfficiencyGap() {
    return efficiencyGap;
  }

  public double getEqualPopulation() {
    return equalPopulation;
  }

  public double getMeanMedianDifference() {
    return meanMedianDifference;
  }
  
  public void setCompactness(double compactness) {
    this.compactness = compactness;
  }

  public void setEfficiencyGap(double efficiencyGap) {
    this.efficiencyGap = efficiencyGap;
  }

  public void setEqualPopulation(double equalPopulation) {
    this.equalPopulation = equalPopulation;
  }

  public void setMeanMedianDifference(double meanMedianDifference) {
    this.meanMedianDifference = meanMedianDifference;
  }
  
  public int getStateId() {
	  return stateId;
  }

  public void setStateId(int stateId) {
    this.stateId = stateId;
  }

  public int getIterations() {
    return iterations;
  }

  public int getDesiredDistricts() {
    return desiredDistricts;
  }

  public void setDesiredDistricts(int desiredDistricts) {
    this.desiredDistricts = desiredDistricts;
  }

  public void setIterations(int iterations) {
    this.iterations = iterations;
  }

  public double getPolsbyPopper() {
    return polsbyPopper;
  }

  public void setPolsbyPopper(double polsbyPopper) {
    this.polsbyPopper = polsbyPopper;
  }

  public double getSchwartzberg() {
    return schwartzberg;
  }

  public void setSchwartzberg(double schwartzberg) {
    this.schwartzberg = schwartzberg;
  }

  public double getSeatsToVotes() {
    return seatsToVotes;
  }

  public void setSeatsToVotes(double seatsToVotes) {
    this.seatsToVotes = seatsToVotes;
  }
}
