package cse308.rockies.redistricting.entities;

public class Merge {

  private Cluster a;
  private Cluster b;

  public Merge(Cluster x, Cluster y){
    a = x;
    b = y;
  }
}
