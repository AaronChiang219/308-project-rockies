package cse308.rockies.redistricting.algorithm;

import cse308.rockies.redistricting.converters.PrecintUpdate;
import cse308.rockies.redistricting.entities.Cluster;
import cse308.rockies.redistricting.entities.District;
import cse308.rockies.redistricting.entities.Precinct;
import cse308.rockies.redistricting.entities.State;
import org.wololo.geojson.GeoJSON;
import org.wololo.jts2geojson.GeoJSONWriter;

import java.util.Random;

public class SimulatedAnnealing {

  private State state;
  private Algorithm algorithm;
  private double temp;
  private double coolingRate;
  private double idealPopulation;

  public SimulatedAnnealing(State state, Algorithm algorithm){
    this.state=state;
    this.algorithm=algorithm;
    idealPopulation=state.getPopulation()/state.getDistricts().size();
    temp=10000;
    coolingRate=0.003;
  }

  public double acceptanceProbability(double oldScore, double newScore, State s) {

    double change = newScore-oldScore;
    if(change<0) {
      return 1.0;
    }
    return Math.exp((change) / temp);
  }

  public void run(){

    System.out.println("Init Scores: ");
    int i=0;

    for (District d:
         state.getDistricts()) {
      i++;
      System.out.println(i + ". District: " + Measures.getNormalizedScore(d, state, algorithm.getWeight()));

      System.out.println("Population: " + d.getPopulation());
      System.out.println("Democrats: " + d.getVoterInfo().getDemocrats());
      System.out.println("Republicans: " + d.getVoterInfo().getRepublicans());
    }

    for (Precinct p: state.getPrecincts()
         ) {
      if(p.getVoterInfo().getRepublicans()>p.getPopulation() || p.getVoterInfo().getDemocrats()>p.getPopulation()){
        System.out.println("Messing up");
      }
    }

    while (temp > 1) {

      District d = state.pickRandomDistrict();
//      District d = state.getDistrictWithLowestScore();
      System.out.println("Size of District d: " + d.getPrecincts().size());

      Precinct p = d.getABorderPrecinct();

      if(p!=null && p.getShape().touches(d.getGeometry())){
        double oldScore = Measures.getNormalizedScore(d, state, algorithm.getWeight());

        District original = p.getDistrict();

        System.out.println("Inital Values********************************************************");

        int b = p.getVoterInfo().getDemocrats();
        int c = p.getVoterInfo().getRepublicans();

        System.out.println("Population: " + d.getPopulation());
        System.out.println("Democrats: " + d.getVoterInfo().getDemocrats());
        System.out.println("Republicans: " + d.getVoterInfo().getRepublicans());

        System.out.println("Population: " + original.getPopulation());
        System.out.println("Democrats: " + original.getVoterInfo().getDemocrats());
        System.out.println("Republicans: " + original.getVoterInfo().getRepublicans());

        System.out.println("Population: " + p.getPopulation());
        System.out.println("Democrats: " + p.getVoterInfo().getDemocrats());
        System.out.println("Republicans: " + p.getVoterInfo().getRepublicans());

//        if(d.getVoterInfo().getRepublicans()<0 || d.getVoterInfo().getDemocrats()<0 ||
//          original.getVoterInfo().getDemocrats()<0 || original.getVoterInfo().getRepublicans()<0)
//          System.out.println("Messing up here");
//
//        if(d.getVoterInfo().getRepublicans()==0 || d.getVoterInfo().getDemocrats()==0 ||
//          original.getVoterInfo().getDemocrats()==0 || original.getVoterInfo().getRepublicans()==0)
//          System.out.println("Messing up here");

        if(original.getPrecincts().size()>80){
          original.removePrecinct(p);
          d.addPrecinct(p);

          System.out.println("After adding************************************************************");

          System.out.println("Population: " + d.getPopulation());
          System.out.println("Democrats: " + d.getVoterInfo().getDemocrats());
          System.out.println("Republicans: " + d.getVoterInfo().getRepublicans());

          System.out.println("Population: " + original.getPopulation());
          System.out.println("Democrats: " + original.getVoterInfo().getDemocrats());
          System.out.println("Republicans: " + original.getVoterInfo().getRepublicans());

          System.out.println("Population: " + p.getPopulation());
          System.out.println("Democrats: " + p.getVoterInfo().getDemocrats());
          System.out.println("Republicans: " + p.getVoterInfo().getRepublicans());

          double newScore = Measures.getNormalizedScore(d, state, algorithm.getWeight());
          boolean acceptableMove;
          if (d.getPopulation()>idealPopulation)
            acceptableMove=false;
          else
            acceptableMove = state.areDistrictsContiguous();
          state.resetVisitedPrecincts();

          if (acceptanceProbability(oldScore, newScore, state) > Math.random() && acceptableMove) {


            d.initBorderPrecincts();
            original.initBorderPrecincts();
            d.setScore(newScore);

            PrecintUpdate pu = new PrecintUpdate(p.getId(), d.getId());
            state.pushDistrictUpdate(algorithm.getSimUpdateList(), pu);
          }else{

            d.removePrecinct(p);
            original.addPrecinct(p);

            System.out.println("After removing**********************************************************");

            System.out.println("Population: " + d.getPopulation());
            System.out.println("Democrats: " + d.getVoterInfo().getDemocrats());
            System.out.println("Republicans: " + d.getVoterInfo().getRepublicans());

            System.out.println("Population: " + original.getPopulation());
            System.out.println("Democrats: " + original.getVoterInfo().getDemocrats());
            System.out.println("Republicans: " + original.getVoterInfo().getRepublicans());

            System.out.println("Population: " + p.getPopulation());
            System.out.println("Democrats: " + p.getVoterInfo().getDemocrats());
            System.out.println("Republicans: " + p.getVoterInfo().getRepublicans());

          }

//          if(d.getVoterInfo().getRepublicans()<0 || d.getVoterInfo().getDemocrats()<0 ||
//            original.getVoterInfo().getDemocrats()<0 || original.getVoterInfo().getRepublicans()<0)
//            System.out.println("Messing up here");
//
//          if(d.getVoterInfo().getRepublicans()==0 || d.getVoterInfo().getDemocrats()==0 ||
//            original.getVoterInfo().getDemocrats()==0 || original.getVoterInfo().getRepublicans()==0)
//            System.out.println("Messing up here");

          if(b!=p.getVoterInfo().getDemocrats() || c!=p.getVoterInfo().getRepublicans()){
            System.out.println("Issues");
          }

        }
      }

      temp *= 1-coolingRate;
    }

    //todo: for testing only
    for (District d: state.getDistricts()
         ) {
      System.out.println("Population: " + d.getPopulation());
      System.out.println("Democrats: " + d.getVoterInfo().getDemocrats());
      System.out.println("Republicans: " + d.getVoterInfo().getRepublicans());
    }

  }


  }

