package cse308.rockies.redistricting.algorithm;

import cse308.rockies.redistricting.entities.*;

import java.util.List;
import java.util.Set;
import org.locationtech.jts.geom.Geometry;
import java.util.ArrayList;
import java.util.Collections;

public class Measures {

	/**
	 * Calculate the change in objective function.
	 * @param oldDistrict
	 * @param newDistrict
	 * @param state
	 * @return The change in objective function. A negative number means lower
	 * score after the change.
	 */
	public static double getObjFunctionChange(District oldDistrict, District newDistrict, State state, Weight weight) {
		double oldScore = getNormalizedScore(oldDistrict, state, weight);
		double newScore = getNormalizedScore(newDistrict, state, weight);
		return newScore - oldScore;
	}

	/**
	 * Returns the normalized objective function score including five measures of a district
	 * @param d District
	 * @param s State where district belongs to
	 * @param w Weight object with desire weight values
	 * @return A normalized objective function score of district
	 */
	public static double getNormalizedScore(District d, State s, Weight w) {
		Geometry geo = d.getGeometry();
		double dArea = geo.getArea();
		double dPerimeter = geo.getLength();
		double compactness1 = polsbyPopper(dArea, dPerimeter);
		double compactness2 = schwartzberg(dArea, dPerimeter);
		double equalPopulation = equalPopulation(s, d);
		double efficiencyGap = efficiencyGap(d.getVoterInfo());
		int numDistricts = s.getDistricts().size();
		double partisanFairness = partisanFairness(s.getVoterInfo(), d.getVoterInfo(), numDistricts);
		double normalizedScore = compactness1 * w.getPolsbyPopper() +
								compactness2 * w.getSchwartzberg() +
								equalPopulation * w.getEqualPopulation() +
								efficiencyGap * w.getEfficiencyGap() +
								partisanFairness * w.getMeanMedianDifference();
		return normalizedScore;
	}

	/**
	 * The Polsby-Popper measure to calculate compactness
	 * @param area Area of district
	 * @param perimeter Perimeter of district
	 * @return Normalized compactness score in Polsby-Popper measure
	 */
	public static double polsbyPopper(double area, double perimeter) {
		double compactness = 4 * Math.PI * (area / (perimeter * perimeter));
		return compactness;
	}

	/**
	 * The Polsby-Popper measure to calculate compactness
	 * @param area Area of district
	 * @param perimeter Perimeter of district
	 * @return Normalized compactness score in Schwartzberg measure
	 */
	public static double schwartzberg(double area, double perimeter) {
		double radius = Math.sqrt(area / Math.PI);
		double equalAreaPerimeter = 2 * Math.PI * radius;
		double compactness = 1 / (perimeter / equalAreaPerimeter);
		return compactness;
	}

	/**
	 * Calculate the standard deviation of district's population
	 * @param s State
	 * @param d District
	 * @return Standard deviation of district's population
	 */
	public static double equalPopulation(State s, District d) {
		int numDistricts = s.getDistricts().size();
		double meanPopulation = s.getPopulation() / numDistricts;
		double sd = Math.abs(d.getPopulation() - meanPopulation);
		double score = sd / s.getPopulation();
		return score;
	}

	/**
	 * Calculate the efficency gap of the given district
	 * @param v Voter information
	 * @return efficency gap score
	 */
	public static double efficiencyGap(VoterInfo v) {
		Parties winParty = v.getDemocrats() > v.getRepublicans() ?
							Parties.DEMOCRATIC : Parties.REPUBLICAN;
		int effectVotes = (int) Math.floor(v.getVotingPopulation() / 2) + 1;
		int winVotes = 0, loseVotes = 0;
		switch (winParty) {
			case DEMOCRATIC:
				winVotes = v.getDemocrats();
				loseVotes = v.getRepublicans();
				break;
			case REPUBLICAN:
				winVotes = v.getRepublicans();
				loseVotes = v.getDemocrats();
				break;
			default:
		}
		double winWaste = Math.abs(winVotes - effectVotes);
		double efficencyGap = Math.abs(winWaste - loseVotes) / (double)v.getVotingPopulation();
		return efficencyGap;
	}

	/**
	 * Calculate the partisan fairness of a district compares to a state
	 * @param vState State election data
	 * @param vDistrict District election data
	 * @param numDistricts Number of districts
	 * @return partisan fairness score of the district
	 */
	public static double partisanFairness(VoterInfo vState, VoterInfo vDistrict, int numDistricts) {
		double meanDemo = vState.getDemocrats() / numDistricts;
		double meanRepub = vState.getRepublicans() / numDistricts;
		double districtDemo = vDistrict.getDemocrats();
		double districtRepub = vDistrict.getRepublicans();
		double fairness = (Math.abs(meanDemo-districtDemo) + Math.abs(meanRepub-districtRepub))/2;
		return fairness;
	}

	// Below are methods for state wide measures
	/**
	 * Return a list of two double value, first is republican, and
	 * second is democrat.
	 */
	public static List<Double> meanMedianDifference(State s) {
            List<Integer> repubVoters = new ArrayList<>();
            List<Integer> demoVoters = new ArrayList<>();
            Set<District> districts = s.getDistricts();
	        for (District d : districts) {
	                VoterInfo v = d.getVoterInfo();
	                repubVoters.add(v.getRepublicans());
	                demoVoters.add(v.getDemocrats());
	         }
	         Collections.sort(repubVoters);
	         Collections.sort(demoVoters);

	         int numDistricts = districts.size();
	         double meanRepub = (double)s.getVoterInfo().getRepublicans() / (double)numDistricts;
	         double meanDemo = (double)s.getVoterInfo().getDemocrats() / (double)numDistricts;
	         int medianIndex = (int) Math.floor(numDistricts / 2);

	        List<Double> result = new ArrayList<>();
	        result.add(new Double(Math.abs((repubVoters.get(medianIndex)-meanRepub) / repubVoters.get(medianIndex))));
	        result.add(new Double(Math.abs((demoVoters.get(medianIndex)-meanDemo) / demoVoters.get(medianIndex))));
            return result;
	}

	public static double statePolsbyPopper(State s) {
		Set<District> districts = s.getDistricts();
		double totalPPScore = 0;
		for (District d : districts) {
			Geometry geo = d.getGeometry();
			double dArea = geo.getArea();
			double dPerimeter = geo.getLength();
			double compactness = polsbyPopper(dArea, dPerimeter);
			totalPPScore += compactness;
		 }
		int numDistricts = districts.size();
		return totalPPScore / numDistricts;
	}

	public static double stateSchwartzberg(State s) {
		Set<District> districts = s.getDistricts();
		double totalPPScore = 0;
		for (District d : districts) {
			Geometry geo = d.getGeometry();
			double dArea = geo.getArea();
			double dPerimeter = geo.getLength();
			double compactness = schwartzberg(dArea, dPerimeter);;
			totalPPScore += compactness;
		 }
		int numDistricts = districts.size();
		return totalPPScore / numDistricts;
	}

	public static double stateEqualPopulation(State s) {
        Set<District> districts = s.getDistricts();
		int numDistricts = s.getDistricts().size();
		double meanPopulation = s.getPopulation() / numDistricts;
		double variance = 0;
		for (District d : districts) {
            variance += Math.abs(d.getPopulation() - meanPopulation);
		}
		//variance = variance / numDistricts;
		//double sd = Math.sqrt(variance);
		//double score = sd / s.getPopulation();
		double score = (variance / numDistricts) / s.getPopulation();
		return score;
	}

	public static double stateEfficiencyGap(State s) {
	  Set<District> districts = s.getDistricts();
    int numDistricts = s.getDistricts().size();
	  double score = 0;
	  for (District d: districts){
	    score += efficiencyGap(d.getVoterInfo());
    }
		return score/numDistricts;
	}

	/**
	 * Return the objective function score of the state
	 */
	public static double stateScore(State s) {
		double pp = statePolsbyPopper(s);
		double schwartzberg = stateSchwartzberg(s);
		double equalPopulation = stateEqualPopulation(s);
		double efficiencyGap = stateEfficiencyGap(s);
		List<Double> mmdList = meanMedianDifference(s);
		double mmd = ( mmdList.get(0) + mmdList.get(1) ) / 2;
		double score = (pp + schwartzberg + equalPopulation +
						efficiencyGap + mmd) / 5;
		return score;
	}

}
