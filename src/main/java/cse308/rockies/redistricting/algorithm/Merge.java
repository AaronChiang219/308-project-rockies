package cse308.rockies.redistricting.algorithm;

import cse308.rockies.redistricting.entities.Cluster;

public class Merge {

  private Cluster a;
  private Cluster b;

  public Merge(Cluster a, Cluster b){
    this.a=a;
    this.b=b;
  }

  public Cluster getA() { return a; }

  public Cluster getB() { return b; }
}
