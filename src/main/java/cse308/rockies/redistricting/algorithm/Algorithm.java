package cse308.rockies.redistricting.algorithm;

import cse308.rockies.redistricting.StatesIterator;
import cse308.rockies.redistricting.converters.PrecintUpdate;
import cse308.rockies.redistricting.converters.UpdateList;
import cse308.rockies.redistricting.entities.*;
import cse308.rockies.redistricting.repositories.PrecinctUpdateRepository;
import cse308.rockies.redistricting.repositories.UpdateListRepository;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.GeometryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

public class Algorithm {

//  @Autowired
//  private UpdateListRepository updateListRepository;

  private static final AtomicInteger count = new AtomicInteger(0);
  private final int id;
  private final State state;
  private int numberOfClusters;
  private final int desiredClusters;
  private int idealPopulation;
  private UpdateList graphUpdateList;
  private UpdateList simUpdateList;
  private double popWeight;
  private Weight weight;


  public Algorithm(Weight weight){
    id=count.incrementAndGet();
    StatesIterator states = StatesIterator.getInstance();
    state = states.getCloneByStateId(weight.getStateId());
    numberOfClusters = state.getClusters().size();
    popWeight = 0.4;
    idealPopulation=0;
    graphUpdateList=new UpdateList();
    simUpdateList=new UpdateList();
    this.weight = weight;
    desiredClusters=weight.getDesiredDistricts();
  }

  public Algorithm(Weight weight, State state) {
    id=count.incrementAndGet();
    StatesIterator states = StatesIterator.getInstance();
    this.state = state;
    numberOfClusters = state.getClusters().size();
    popWeight = 0.4;
    idealPopulation=0;
    graphUpdateList=new UpdateList();
    this.weight = weight;
    desiredClusters=weight.getDesiredDistricts();
  }

  public UpdateList getGraphUpdateList(){
    return graphUpdateList;
  }

  public UpdateList getSimUpdateList(){ return simUpdateList;}

  public void run(){
    runPhaseOne();
    state.convertClustersToDistricts(this.weight);
  }

  public void runPhaseOne(){
    int iteration=0;
    while (numberOfClusters>desiredClusters){
      checkequality();

      idealPopulation = state.determineIdealPopulation();
      boolean determined = determineCandidatePairs();

      if(determined){
        joinCandidatePairs();

        iteration++;
        state.pushUpdate(graphUpdateList, iteration);
        removeUnusedClusters();
      }else{
        popWeight+=.05;
      }


    }
    graphUpdateList.setStatus(1);

    System.out.println("Done with phase one");



  }

  public void runPhaseTwo(){
    SimulatedAnnealing simulatedAnnealing = new SimulatedAnnealing(state, this);
    simulatedAnnealing.run();
    simUpdateList.setStatus(1);
  }


  private void checkequality() {
    for (Cluster clusterB: state.getClusters()
         ) {
      if(clusterB.getNeighbors().size()!=clusterB.getEdges().size())//|| clusterB.getNeighbors().size()!=clusterB.getEdgeMap().size())
        System.out.println("Not equal");
    }

  }

  private void removeUnusedClusters() {

    for (Cluster c: state.getClusters()
         ) {
      Set<Cluster> toRemove = new HashSet<>();
      for (Cluster n: c.getNeighbors()
           ) {
        if(!(state.getClusters().contains(n))){
          toRemove.add(n);
          c.getEdgeMap().remove(n);
        }
      }

      SortedSet<ClusterEdge> edgesRemoval = new TreeSet<>();

      for (ClusterEdge ce: c.getEdges()
           ) {
        if(!(toRemove.contains(ce.getB()))){
          edgesRemoval.add(ce);
        }
      }

      c.getNeighbors().removeAll(toRemove);
      c.setEdges(edgesRemoval);

//      System.out.println(c);
    }
  }

  private boolean determineCandidatePairs(){
    int i =0;

    boolean determined = false;

    for (Cluster c: state.getClusters()) {
      System.out.println(i + ". Using " + c.getId());
      if(numberOfClusters>desiredClusters){
        if(c.isUsedInIteration()==false && c.getPopulation()<(popWeight*idealPopulation)){
          System.out.println("Not used; getting neighbor");
//          Cluster neighbor = c.getMostAttractiveNeighbor().getB();

          Cluster neighbor = state.getMostAttractiveNeighbor(c);

//          Cluster neighbor = c.getANeighbor();
          if(neighbor!=null && state.getClusters().contains(neighbor)){

            System.out.println("Neighbor found");
            if(!(state.getClusters().contains(neighbor)))
              System.out.println("************************************************************************************Not in clusterslist");

            if(neighbor.isUsedInIteration()==false && neighbor.getPopulation()<(popWeight*idealPopulation)){
              System.out.println("Neighbor not used either");

              determined=true;

              c.setUsedInIteration(true);
              neighbor.setUsedInIteration(true);

              Merge merge = new Merge(c, neighbor);
              state.addToMerges(merge);

              System.out.println("Added to merges: (Cluster: " + c.getId() + " ; Cluster: " + neighbor.getId());
              numberOfClusters--;
            }
          }else{
//            c.removeNeighbor(neighbor);
            System.out.println("Neighbor not found");
          }

        }
      }
      i++;
    }


    System.out.println("Finished determining");

    return determined;
  }

  private void joinCandidatePairs(){
    Map<Cluster, Cluster> edgeMerges = new HashMap<>();

    for (Merge m: state.getMerges()) {
      Cluster clusterA = m.getA();
      Cluster clusterB = m.getB();

      if(clusterA.getNeighbors().size()!=clusterA.getEdges().size() || clusterA.getNeighbors().size()!=clusterA.getEdgeMap().size())
        System.out.println("Not equal");

      if(clusterB.getNeighbors().size()!=clusterB.getEdges().size() || clusterB.getNeighbors().size()!=clusterB.getEdgeMap().size())
        System.out.println("Not equal");

//      clusterA.getEdges().addAll(clusterB.getEdges());
      edgeMerges.put(clusterA, clusterB);

      mergeClusterInfo(clusterA, clusterB);
      mergeClusterPrecincts(clusterA, clusterB);
//      mergeClusterEdges(clusterA, clusterB);

      clusterA.mergeClusterEdges(clusterB);
      mergeClusterNeighbors(clusterA, clusterB);
//      clusterA.mergeClusterEdges(clusterB);

      clusterA.setUsedInIteration(false);

//      removeClusterB(clusterB);
      state.getClusters().remove(clusterB);

      System.out.println("Number of Clusters: " + state.getClusters().size());

    }

//    for (Map.Entry<Cluster, Cluster> entry : edgeMerges.entrySet()) {
//      entry.getKey().mergeClusterEdges(entry.getValue());
//    }

    state.getMerges().clear();

  }

  private void removeClusterB(Cluster clusterA) {
    state.getClusters().remove(clusterA);

    Map<Cluster, ClusterEdge> edgesToRemove = new HashMap<>();

    for (ClusterEdge ce: clusterA.getEdges()
         ) {
      Cluster a = ce.getB();
      a.removeFromNeighbors(clusterA);
      edgesToRemove.put(a, ce);
    }

    for (Map.Entry<Cluster, ClusterEdge> entry : edgesToRemove.entrySet()) {
      entry.getKey().removeFromEdges(entry.getValue());
    }


  }

  private void updateClustersGeometry() {
    for (Cluster c: state.getClusters()) {
      List<Geometry> geometries = new ArrayList<>();

      for (Precinct p: c.getPrecincts()) {
        geometries.add(p.getShape());
      }

      GeometryFactory factory = new GeometryFactory();
      GeometryCollection combinedGeo = (GeometryCollection)factory.buildGeometry(geometries);

      c.setShape(combinedGeo.union());

    }
  }

  private void mergeClusterInfo(Cluster clusterA, Cluster clusterB) {
    //update population
    clusterA.setPopulation(clusterA.getPopulation()+clusterB.getPopulation());

    clusterA.getVoterInfo().merge(clusterB.getVoterInfo());

    List<Geometry> geometries = new ArrayList<>();
    geometries.add(clusterA.getShape());
    geometries.add(clusterB.getShape());

    GeometryFactory factory = new GeometryFactory();
    GeometryCollection combinedGeo = (GeometryCollection)factory.buildGeometry(geometries);

    clusterA.setShape(combinedGeo.union());
  }

  private void mergeClusterPrecincts(Cluster clusterA, Cluster clusterB) {
    //update precincts
    Set<Precinct> clusterAPrecincts = clusterA.getPrecincts();
    Set<Precinct> clusterBPrecincts = clusterB.getPrecincts();

    clusterAPrecincts.addAll(clusterBPrecincts);
    clusterB.redirectPrecincts(clusterA);
  }

  private void mergeClusterNeighbors(Cluster clusterA, Cluster clusterB) {
    //update neighbors


//    for (Cluster c: clusterBNeighbors
//         ) {
//      if(c!=clusterA){
//        c.getNeighbors().remove(clusterB);
//        c.getNeighbors().add(clusterA);
//      }
//
//    }
  }

  private void mergeClusterEdges(Cluster clusterA, Cluster clusterB) {
    //update edges


//

//    ClusterEdge edgeA = clusterA.removeEdgeForCluster(clusterB);
//    ClusterEdge edgeB = clusterB.removeEdgeForCluster(clusterA);
//
//    SortedSet<ClusterEdge> clusterAEdges = clusterA.getEdges();
//    SortedSet<ClusterEdge> clusterBEdges = clusterB.getEdges();
//
//    clusterAEdges.remove(edgeA);
//    clusterBEdges.remove(edgeB);
//
//    System.out.println(clusterAEdges);
//    System.out.println(clusterBEdges);
//
//    SortedSet<ClusterEdge> newEdges = new TreeSet<>();
//
//    newEdges.addAll(clusterAEdges);
//    clusterB.redirectEdges(clusterA);
//    newEdges.retainAll(clusterBEdges);
//    clusterBEdges.removeAll(newEdges);
//
//    clusterAEdges.removeAll(newEdges);
//
//    clusterAEdges.addAll(clusterBEdges);
//    clusterAEdges.addAll(newEdges);
////    clusterA.mergeClusterEdges(clusterB);
//
//    clusterA.redirectEdges(clusterA);
////
////    ClusterEdge ce = clusterA.getCommonEdge(clusterB);
////    ClusterEdge ce2 = clusterB.getCommonEdge(clusterA);
////
////    if(ce!=null)
////      clusterAEdges.remove(ce);
////
////    if(ce2!=null)/
////      clusterAEdges.remove(ce2);
//
//    updateEdgeAttraction(clusterA, edgeA.getCountyAttraction());
  }

  private void updateEdgeAttraction(Cluster a, double countyAttraction) {
    for (ClusterEdge ce: a.getEdges()) {
      ce.setCountyAttraction(ce.getCountyAttraction()*countyAttraction);
    }
  }


  public int saveMap(UpdateListRepository updateListRepository, PrecinctUpdateRepository precinctUpdateRepository, User user){
    //TODO: test save
    UpdateList ul = state.pushFinalValues(user);
    for (PrecintUpdate pu: ul.getUpdates() ) {
      precinctUpdateRepository.save(pu);
    }
    updateListRepository.save(ul);
    return ul.getId();
  }
  
  public State getState() {
	  return state;
  }

  public Cluster createClusterForPrecinctMove(Cluster c, Precinct p){
    Cluster newCluster = new Cluster();

    return newCluster;
  }

  public District districtMove(District d, Precinct p) {
    return new District(d, p);
  }

  public Weight getWeight(){ return weight; }
}
