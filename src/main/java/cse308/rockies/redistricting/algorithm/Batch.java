package cse308.rockies.redistricting.algorithm;

import com.fasterxml.jackson.databind.util.JSONPObject;
import cse308.rockies.redistricting.converters.BatchResults;
import cse308.rockies.redistricting.converters.BatchSummary;
import cse308.rockies.redistricting.entities.State;
import cse308.rockies.redistricting.entities.User;
import cse308.rockies.redistricting.entities.Weight;
import cse308.rockies.redistricting.repositories.PrecinctUpdateRepository;
import cse308.rockies.redistricting.repositories.UpdateListRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Batch {

  private List<Weight> weights;
  private List<Integer> batchResult;
  private ArrayList<BatchSummary> resultSummaries;
  private int runNum;

  /**
   * Takes a Weight object with number of batch and range of variance, and generate  a list of
   * Weight object, with size equals to number of batch to weights list.
   * @param weight
   */
  public Batch(Weight weight){
	int numBatch = weight.getIterations();
	if (numBatch <=0 ) weights = null;
	if (numBatch == 1) weights.add(weight);
	else {
		int stateId = weight.getStateId();
		double variance = 0;
		double increment = 2 * variance / (numBatch - 1);
		weights = new ArrayList<>();
		for (int i = 0; i < numBatch; i++) {
			Weight newWeight = new Weight(stateId, numBatch);
			newWeight.setCompactness(weight.getCompactness() - variance + increment * i);
			newWeight.setEfficiencyGap(weight.getEfficiencyGap() - variance + increment * i);
			newWeight.setEqualPopulation(weight.getEqualPopulation() - variance + increment * i);
			newWeight.setMeanMedianDifference(weight.getMeanMedianDifference() - variance + increment * i);
			weights.add(newWeight);
		}
	}

    runNum=0;
  }

  public Batch(Weight minWeight, Weight maxWeight) {
    int numBatch = minWeight.getIterations();

    batchResult = new ArrayList<>();
    resultSummaries = new ArrayList<>();

    if (numBatch <=0 ) weights = null;
    if (numBatch == 1) weights.add(minWeight);
    else {
      int stateId = maxWeight.getStateId();
      weights = new ArrayList<>();
      for (int i = 0; i < numBatch; i++) {
        Weight newWeight = new Weight(stateId, numBatch);

        newWeight.setCompactness(minWeight.getCompactness() +
          (maxWeight.getCompactness()-minWeight.getCompactness()*Math.random()));

        newWeight.setEfficiencyGap(minWeight.getEfficiencyGap() +
          (maxWeight.getEfficiencyGap()-minWeight.getEfficiencyGap()*Math.random()));

        newWeight.setEqualPopulation(minWeight.getEqualPopulation() +
          (maxWeight.getEqualPopulation()-minWeight.getEqualPopulation()*Math.random()));

        newWeight.setMeanMedianDifference(minWeight.getMeanMedianDifference() +
          (maxWeight.getMeanMedianDifference()-minWeight.getMeanMedianDifference()*Math.random()));

        weights.add(newWeight);
      }
    }

    runNum=0;

  }

  /**
   * Do batch according to the weights list in this controller class.
   */
  public void doBatch(UpdateListRepository u, PrecinctUpdateRepository precinctUpdateRepository, User user){
    if (weights == null) return;
    int numRun = weights.size();

    for (int i=0; i < numRun; i++) {
      Algorithm algorithm = new Algorithm(weights.get(i));
      algorithm.run();
      algorithm.runPhaseTwo();

      int databaseId = algorithm.saveMap(u, precinctUpdateRepository, user);
      batchResult.add(databaseId);

      State result = algorithm.getState();
      BatchSummary summary = result.getSummary(runNum+1, databaseId);
      resultSummaries.add(summary);



      //update RUN number
      runNum++;
    }
  }

  /**
   * Return the result map selected in JSON
   * @param resultIndex
   * @return Result map selected in JSON
   */
  public JSONPObject getResult(int resultIndex) {
    int mapId = batchResult.get(resultIndex);
    return loadMapFromDB(mapId);
  }

  /**
   * Load map from database by mapId
   * @param mapId
   * @return A JSON object of the map
   */
  private JSONPObject loadMapFromDB(int mapId) {
    return null;
  }

  public int getRunNumber(){
    return runNum;
  }

  public String finalResult(){
    return "";
  }

  public ArrayList<BatchSummary> getBatchResult(){
      return resultSummaries;
  }
}
