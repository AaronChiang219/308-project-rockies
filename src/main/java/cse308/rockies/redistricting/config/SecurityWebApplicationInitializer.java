package cse308.rockies.redistricting.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityWebApplicationInitializer
  extends AbstractSecurityWebApplicationInitializer {

}
