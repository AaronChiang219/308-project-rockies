package cse308.rockies.redistricting.controllers;

import cse308.rockies.redistricting.entities.UserRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {

  @Autowired
  JdbcUserDetailsManager jdbcUserDetailsManager;

  @Autowired
  BCryptPasswordEncoder encoder;

  @RequestMapping(value = "/user/register", method = RequestMethod.POST)
  public @ResponseBody
  UserRegistration RegisterUser(@RequestBody UserRegistration newRegistration){
    List<GrantedAuthority> authorities = new ArrayList<>();
    authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
    String encrypted_password = encoder.encode(newRegistration.getPassword());
    User user = new User(newRegistration.getUsername(), encrypted_password, authorities);
    jdbcUserDetailsManager.createUser(user);
    return newRegistration;
  }
}
