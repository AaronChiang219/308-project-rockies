package cse308.rockies.redistricting.controllers;

import cse308.rockies.redistricting.converters.UpdateList;
import cse308.rockies.redistricting.entities.State;
import cse308.rockies.redistricting.entities.User;
import cse308.rockies.redistricting.repositories.StateRepository;
import cse308.rockies.redistricting.repositories.UpdateListRepository;
import cse308.rockies.redistricting.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class StateController {
  @Autowired
  private StateRepository stateRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UpdateListRepository updateListRepository;

  @RequestMapping(value = "/api/state", method = RequestMethod.GET)
  public void addState(){

    System.out.println("Handling");

    Optional o = stateRepository.findById(9);

    State s = (State)o.get();

    System.out.println(s);
  }

  @RequestMapping(value = "/api/state/maps", method = RequestMethod.POST)
  public @ResponseBody
  List<String> getMaps(@RequestBody String username){
    User user = userRepository.findByUsername(username);

    List<UpdateList> maps = new ArrayList<>(updateListRepository.findAllByUser(user));
    List<String> mapNames = new ArrayList<>();
    for(UpdateList u: maps){
      mapNames.add(String.valueOf(u.getId()));
    }

    return mapNames;
  }

  @RequestMapping(value = "/api/state/load", method = RequestMethod.POST)
  public @ResponseBody
  UpdateList loadState(@RequestBody int id){
    UpdateList ul = updateListRepository.findById(id).get();
    return ul;
  }

  @RequestMapping(value = "/api/state/remove", method = RequestMethod.POST)
  public @ResponseBody
  void removeState(@RequestBody int id){
    updateListRepository.deleteById(id);
  }
}
