package cse308.rockies.redistricting.controllers;

import com.fasterxml.jackson.databind.util.JSONPObject;
import cse308.rockies.redistricting.algorithm.Algorithm;
import cse308.rockies.redistricting.algorithm.Batch;
import cse308.rockies.redistricting.converters.BatchResults;
import cse308.rockies.redistricting.converters.BatchSummary;
import cse308.rockies.redistricting.entities.State;
import cse308.rockies.redistricting.entities.User;
import cse308.rockies.redistricting.entities.Weight;
import java.util.ArrayList;
import java.util.List;

import cse308.rockies.redistricting.repositories.PrecinctUpdateRepository;
import cse308.rockies.redistricting.repositories.UpdateListRepository;
import cse308.rockies.redistricting.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BatchController {

  @Autowired
  private UpdateListRepository updateListRepository;

  @Autowired
  private PrecinctUpdateRepository precinctUpdateRepository;

  @Autowired
  private UserRepository userRepository;

  Batch batch;
  ArrayList<BatchSummary> summaries;

  @RequestMapping(value = "/api/batch", method = RequestMethod.POST)
  public @ResponseBody
  Weight createWeights(@RequestBody ArrayList<Weight> weight){
    batch = new Batch(weight.get(0), weight.get(1));
    summaries = batch.getBatchResult();
    //batch.doBatch();
    return weight.get(0);
  }

  @RequestMapping(value = "/api/batch/start", method = RequestMethod.POST)
  public void startBatch(@RequestBody String username){
    User user = userRepository.findByUsername(username);
    System.out.println(username);
    batch.doBatch(updateListRepository, precinctUpdateRepository, user);
  }

  @RequestMapping(value = "/api/batch/update", method = RequestMethod.GET)
  public @ResponseBody
  ArrayList<BatchSummary> getBatchResults(){
    ArrayList<BatchSummary> summariesClone = new ArrayList<>();

    for (BatchSummary bs: summaries ) {
      summariesClone.add(bs);
    }

    if(summaries.size()>0){
      System.out.println("Here");
    }
    summaries.clear();


    return summariesClone;
  }



}
