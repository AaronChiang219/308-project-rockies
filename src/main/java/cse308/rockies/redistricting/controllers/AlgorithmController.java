package cse308.rockies.redistricting.controllers;

import cse308.rockies.redistricting.algorithm.Algorithm;
import cse308.rockies.redistricting.converters.PrecintUpdate;
import cse308.rockies.redistricting.converters.UpdateList;
import cse308.rockies.redistricting.entities.State;
import cse308.rockies.redistricting.entities.User;
import cse308.rockies.redistricting.entities.Weight;
import cse308.rockies.redistricting.repositories.PrecinctUpdateRepository;
import cse308.rockies.redistricting.repositories.StateRepository;
import cse308.rockies.redistricting.repositories.UpdateListRepository;
import cse308.rockies.redistricting.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class AlgorithmController {
  private Weight weight;
  private Algorithm algorithm;
  private UpdateList graphUpdateList;
  private UpdateList simUpdateList;

  @Autowired
  private UpdateListRepository updateListRepository;

  @Autowired
  private PrecinctUpdateRepository precinctUpdateRepository;

  @Autowired
  private StateRepository stateRepository;

  @Autowired
  private UserRepository userRepository;

  private User user;

  @RequestMapping(value = "/api/algorithm", method = RequestMethod.POST)
  public @ResponseBody
  Weight createWeights(@RequestBody Weight newWeight){
    this.weight = newWeight;
//    State state = stateRepository.findById(weight.getStateId()).get();
    this.algorithm = new Algorithm(weight);
    graphUpdateList=algorithm.getGraphUpdateList();
    simUpdateList=algorithm.getSimUpdateList();
//    algorithm.run();

    return newWeight;
  }


  @RequestMapping(value = "/api/algorithm/start", method = RequestMethod.POST)
  public void startAlgorithm(@RequestBody String username){
    user = userRepository.findByUsername(username);
    System.out.println(username);
    algorithm.run();
  }

  @RequestMapping(value = "/api/algorithm/update/graph", method = RequestMethod.GET)
  public @ResponseBody
  UpdateList updateGUIGraph(){
//    this.weight = newWeight;
//    this.algorithm = new Algorithm(weight);
//    algorithm.run();
    UpdateList updates =null;
    if(graphUpdateList.getUpdates().size()>0){
      System.out.println("This");
    }
    try {
      updates = graphUpdateList.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }

    graphUpdateList.clearUpdates();
    return updates;
  }

  @RequestMapping(value = "/api/algorithm/start/sim", method = RequestMethod.GET)
  public void startSimulatedAnnealing() {
    algorithm.runPhaseTwo();
  }

  @RequestMapping(value = "/api/algorithm/update/sim", method = RequestMethod.GET)
  public @ResponseBody
  UpdateList updateGUISim(){
//    this.weight = newWeight;
//    this.algorithm = new Algorithm(weight);
//    algorithm.run();
    UpdateList updates =null;

    try {
      updates = simUpdateList.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }

    simUpdateList.clearUpdates();
    return updates;
  }

  @RequestMapping(value = "/api/algorithm/save", method = RequestMethod.GET)
  public void saveMap() {
    algorithm.saveMap(updateListRepository, precinctUpdateRepository, user);
  }


}
