package cse308.rockies.redistricting;

import cse308.rockies.redistricting.entities.State;
import cse308.rockies.redistricting.repositories.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;

public class StatesIterator {
  private static StatesIterator statesIterator;
  private Iterable<State> states;

  private StatesIterator(){
    states = null;
  }

  public static StatesIterator getInstance(){
    if(statesIterator==null)
      statesIterator=new StatesIterator();
    return statesIterator;
  }

  public void setStates(Iterable<State> states){
    this.states=states;
  }

  public Iterable<State> getStates(){
    return states;
  }

  public State getStateById(int stateId){
    for (State s: states) {
      if(s.getId()==stateId)
        return s;
    }
    return null;
  }

  public State getCloneByStateId(int stateId){
    for (State s: states) {
      if(s.getId()==stateId) {
        try {
          return s.clone();
        } catch (CloneNotSupportedException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }
}
